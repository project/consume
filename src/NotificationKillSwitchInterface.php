<?php

namespace Drupal\consume;

/**
 * Interface for creating a notification kill switch class.
 *
 * @see \Drupal\consume\NotificationKillSwitch
 * @see consume_mail_alter()
 */
interface NotificationKillSwitchInterface {

  /**
   * Set the mail IDs of the notification messages to suppress.
   *
   * @param string[] $mail_ids
   *   List of email notification IDs to suppress when kill switch is activated.
   *
   * @see \Drupal\Core\Mail\MailManagerInterface::mail()
   */
  public function setSuppressId(array $mail_ids = []): void;

  /**
   * Gets the active status of the notification suppression switch.
   *
   * @return bool
   *   TRUE if this kill switch is set to "on" (i.e. suppress messages).
   */
  public function isSuppressing(): bool;

  /**
   * Checks if a message with this message ID should be suppressed?
   *
   * @return bool
   *   Returns TRUE if the message ID should be suppressed AND notification
   *   suppression is turned on.
   */
  public function shouldSuppress($message_id): bool;

  /**
   * Turn on the notification suppression for this instance.
   */
  public function suppress(): void;

  /**
   * Turn off the notification suppression for this instance.
   */
  public function unsuppress(): void;

}
