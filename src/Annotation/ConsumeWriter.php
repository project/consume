<?php

namespace Drupal\consume\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Consume import data writers.
 *
 * Plugin Namespace: Plugin\Consume\Writer.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\consume\Import\Persist\WriterInterface
 * @see \Drupal\consume\ConsumeWriterPluginManager
 *
 * @ingroup consume_writer_plugins
 *
 * @Annotation
 */
class ConsumeWriter extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public $id;

  /**
   * Display label for this plugin.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $label;

}
