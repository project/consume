<?php

namespace Drupal\consume\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Consume import data mappers.
 *
 * Plugin Namespace: Plugin\Consume\Mapper.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\consume\Import\Mapper\DataMapperInterface
 * @see \Drupal\consume\ConsumeMapperPluginManager
 *
 * @ingroup consume_mapper_plugins
 *
 * @Annotation
 */
class ConsumeMapper extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public $id;

  /**
   * Display label for this plugin.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public $label;

}
