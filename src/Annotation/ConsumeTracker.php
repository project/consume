<?php

namespace Drupal\consume\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Consume import tracker.
 *
 * Plugin Namespace: Plugin\Consume\Tracker.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\consume\Import\Tracker\TrackerInterface
 * @see \Drupal\consume\ConsumeTrackerPluginManager
 *
 * @ingroup consume_tracker_plugins
 *
 * @Annotation
 */
class ConsumeTracker extends Plugin {

  /**
   * The ID of the plugin.
   *
   * @var string
   */
  public $id;

}
