<?php

namespace Drupal\consume\Command;

use Drupal\consume\ConsumeImporterManager;
use Drupal\consume\Consumer;
use Drupal\consume\ConsumerInterface;
use Drupal\consume\Import\BatchInterface;
use Drupal\consume\Import\Error\ErrorHandlerInterface;
use Drupal\consume\Import\Error\LogErrorHandler;
use Drupal\consume\Import\Exception\BatchAlreadyRunningException;
use Drupal\consume\Import\ExclusiveImporterInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Tracker\HashingTrackerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Utility\Error;
use Drush\Commands\DrushCommands;

/**
 * Base class to expose the common batch importer functionality for Drush.
 */
abstract class ImportCommandBase extends DrushCommands {

  /**
   * The importer manager.
   *
   * @var \Drupal\consume\ConsumeImporterManager
   */
  protected ConsumeImporterManager $importManager;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * Creates a new instance an consume importer based Drush command.
   *
   * @param \Drupal\consume\ConsumeImporterManager $import_manager
   *   The importer strategy manager service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface|null $account_switcher
   *   The account switcher interface.
   */
  public function __construct(ConsumeImporterManager $import_manager, AccountSwitcherInterface $account_switcher = NULL) {
    $this->importManager = $import_manager;
    $this->accountSwitcher = $account_switcher;
  }

  /**
   * Get the ID of the importer strategy used by this Drush command.
   *
   * @return string
   *   The importer strategy's ID.
   */
  abstract public function getImporterId(): string;

  /**
   * Get the main importer object that this Drush command works with.
   *
   * @return \Drupal\consume\Import\ImporterInterface
   *   The importer this set of commands works with.
   */
  public function getImporter(): ImporterInterface {
    $importId = $this->getImporterId();
    return $this->importManager->getInstance($importId);
  }

  /**
   * Get the user ID of the user to run the import process as.
   *
   * @return int|null
   *   The user ID of the user to run the import process as. Returning NULL
   *   means no user context change should happen, or 0 means run as anonymous.
   */
  public function getImportUserId(): ?int {
    return NULL;
  }

  /**
   * Get the error handler that should be used with the consumer execution.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer to build the error handler for.
   *
   * @return \Drupal\consume\Import\Error\ErrorHandlerInterface
   *   The import process error handler.
   */
  protected function getErrorHandler(ConsumerInterface $consumer): ErrorHandlerInterface {
    return new LogErrorHandler($consumer);
  }

  /**
   * Execute an importer.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer instance to run.
   * @param array $datasources
   *   The datasources to attach to import operations.
   * @param string[]|null $op_names
   *   A list of operation names to run. If empty or NULL, all available
   *   importer operations will be run.
   */
  public function runImport(ImporterInterface $importer, array $datasources, ?array $op_names = NULL): void {
    try {
      $importer->setup();
      $consumer = new Consumer($importer, $this->accountSwitcher);
      $consumer
        ->setErrorHandler($this->getErrorHandler($consumer))
        ->setImportUser($this->getImportUserId())
        ->setDatasources($datasources);

      // Determine if the importer should be run as a batch or as a normal
      // single pass process.
      if ($importer instanceof BatchInterface) {
        batch_set($importer->buildBatch($consumer, $op_names));
        $batch = &batch_get();
        $batch['progressive'] = FALSE;
        drush_backend_batch_process();
      }
      else {
        $importer->execute($consumer, $op_names);
        $importer->finalize();
      }
    }
    catch (BatchAlreadyRunningException $e) {
      $this->logger()->error(<<<EOT
        This import may already be running. This process has been stopped to avoid
        interfering with an existing process execution.

        Wait for this command to complete, or use the "release-lock" command to
        remove this process lock and allow running of the process.
        EOT
      );
    }
    catch (\Throwable $e) {
      $this->logger()->error(Error::DEFAULT_ERROR_MESSAGE, Error::decodeException($e));
      // Ensure that importer has a chance to close any resources properly
      // and is able to do any required clean-up work.
      $importer->finalize();
    }
  }

  /**
   * Release the exclusive lock for the importer.
   *
   * @param bool $use_prompt
   *   Should the user be prompted before releasing the content lock.
   */
  public function releaseLock(bool $use_prompt = TRUE): void {
    $importer = $this->getImporter();

    // If ensure that the importer is an exclusive batch importer.
    if ($importer instanceof ExclusiveImporterInterface) {
      if ($use_prompt) {
        $question = sprintf('Another "%s" process may already be running. Are you sure you wish to release this lock? It may interrupt any current processes.', $importer->getTitle());
        $answer = $this->askDefault($question, 'y');
        $unlock = preg_match('/y(es)?/i', $answer);
      }
      else {
        $unlock = TRUE;
      }

      if ($unlock) {
        $importer->releaseLock();
        $this->output()->writeln('The process lock has been released.');
      }
    }
    else {
      $this->output()->writeln('This import process does not use locks and does not need to be released.');
    }
  }

  /**
   * Find and clear all the hashing trackers for the encapsulated importer.
   */
  public function clearTrackingHash(): void {
    $importer = $this->getImporter();

    foreach ($importer->getOperations() as $op) {
      if ($op instanceof ImportOperationInterface) {
        $tracker = $op->getTracker();
        $tracker->setup($importer, $op);

        if ($tracker instanceof HashingTrackerInterface) {
          $tracker->clearHash();
        }
      }
    }
  }

}
