<?php

namespace Drupal\consume\Import;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\consume\ConsumerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\StrategyBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Object for building a set of batch operations for running a single import.
 */
class BatchImporter extends StrategyBase implements BatchInterface, ContainerInjectionStrategyInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;
  use LoggerChannelTrait;
  use MessengerTrait;

  /**
   * Should import exit on any data process, mapping or write error.
   *
   * Parsing errors are not captured and will mean that the import will
   * not be able to continue - so those errors will still exit regardless of
   * this flag.
   *
   * This only indicates that the batch can continue with other rows if possible
   * this entry fails. In most cases this might be if a row fails to validate,
   * and is unrelated to other data.
   *
   * @var bool
   */
  protected $exitOnError = TRUE;

  /**
   * A UUID to identify this batch instance, and runtime.
   *
   * @var string
   */
  protected $batchId;

  /**
   * The batch display title.
   *
   * @var string
   */
  protected $title;

  /**
   * The state service for tracking the batch process.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The datetime's time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Set of importer operations to run for this import.
   *
   * @var \Drupal\consume\Import\ImportOperationInterface[]
   */
  protected $operations = [];

  /**
   * Number of items that can be run in a single batch.
   *
   * @var int
   */
  public $batchSize = 50;

  /**
   * Create new batchable importer instance.
   *
   * @param string $id
   *   The importer instance ID.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The importer definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service for recording the batch process.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID generator.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime's time service.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, StateInterface $state, UuidInterface $uuid, TimeInterface $time) {
    $this->id = $id;
    $this->definition = $definition;

    $this->batchSize = $definition->batch_size ?? $this->batchSize;

    // Allow the import process to keep going even if a single skippable
    // failure was detected. Severe errors cannot be skipped and will still
    // halt the import process.
    if ($definition->allow_skippable_errors ?? FALSE) {
      $this->exitOnError = FALSE;
    }

    $this->batchId = $uuid->generate();
    $this->title = $definition->title ?? $id;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    return new static(
      $id,
      $definition,
      $container->get('state'),
      $container->get('uuid'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBatchId(): string {
    return $this->batchId;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->title ?? $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getLockId(): string {
    return 'consume_importer:' . $this->id();
  }

  /**
   * Gets the key to use with the state to fetch the import status.
   *
   * @return string
   *   The key to use to get the status info for this importer instance.
   */
  protected function getStateStatusKey(): string {
    return 'consume.importer_status.' . $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusInfo(): array {
    $stateKey = $this->getStateStatusKey();
    return $this->state->get($stateKey, []);
  }

  /**
   * {@inheritdoc}
   */
  public function updateStatus(OperationInterface $operation, $opStatus): void {
    $status = $this->getStatusInfo();
    $opName = $operation->getName();

    // Update the operation information status information if any was provided.
    if ($opStatus) {
      $status['operations'][$opName] = $opStatus;
    }
    else {
      unset($status['operations'][$opName]);
    }

    // Update the last completed phase of the import process.
    $status['phase'] = $opName;
    $this->state->set($this->getStateStatusKey(), $status);
  }

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    if ($this instanceof ExclusiveImporterInterface) {
      $this->acquireLock();
    }

    // Initialize all the Import operations in preparation of ahead of starting
    // the batch import process.
    foreach ($this->operations as $op) {
      $op->setup($this);
    }

    // Clear existing status information and ensure fresh status data.
    $this->state->set($this->getStateStatusKey(), [
      'start' => $this->time->getRequestTime(),
      'finish' => NULL,
      'phase' => 'init',
      'operations' => [],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function addOperation(string $name, OperationInterface $operation): self {
    $this->operations[$name] = $operation;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(): array {
    return $this->operations;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ConsumerInterface $consumer, ?array $op_names = NULL): void {
    $operations = $this->getOperations();
    if ($op_names) {
      $operations = array_intersect_key($operations, array_fill_keys($op_names, TRUE));
    }

    foreach ($operations as $op) {
      $op($consumer);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function finalize(): void {
    if ($this instanceof ExclusiveImporterInterface) {
      $this->releaseLock();
    }
  }

  /* ---------------- Batch Interface ---------------- */

  /**
   * {@inheritdoc}
   */
  public function getBatchSize(): int {
    return $this->batchSize;
  }

  /**
   * {@inheritdoc}
   */
  public function getProgressMessage() {
    return $this->t('Completed @current of @total.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildBatch(ConsumerInterface $consumer, ?array $op_names = NULL): array {
    $operations = $this->getOperations();
    if ($op_names) {
      $operations = array_intersect_key($operations, array_fill_keys($op_names, TRUE));
    }

    $batchOps = [];
    foreach ($operations as $op) {
      $batchOps[] = [$op, [$consumer]];
    }

    return [
      'title' => $this->getTitle(),
      'progress_message' => $this->getProgressMessage(),
      'operations' => $batchOps,
      'progressive' => $this->getBatchSize() !== -1,
      'finished' => [$this, 'batchFinish'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function batchFinish($success, array $results, array $operations): void {
    $this->finalize();

    if ($success) {
      $stateKey = $this->getStateStatusKey();
      $status = $this->state->get($stateKey, []);
      $status['phase'] = 'completed';
      $status['finish'] = $this->time->getCurrentTime();
      $this->state->set($stateKey, $status);

      $build = [
        '#theme' => 'item_list',
        '#title' => $this->getTitle(),
        '#items' => [],
      ];
      foreach ($this->getOperations() as $name => $operation) {
        if (isset($results[$name])) {
          $build['#items'][] = $this->t('@operation: @result', [
            '@operation' => $operation->getTitle(),
            '@result' => $operation->formatResult($results[$name]),
          ]);
        }
      }

      $html = \Drupal::service('renderer')->renderRoot($build);
      $this->messenger()->addStatus($html);
      $this->getLogger('consume.' . $this->id())->info($html);
    }
    else {
      $build = [
        '#theme' => 'item_list',
        '#title' => $this->t('This batch did not complete the following operations:'),
        '#items' => [],
      ];

      foreach ($operations as $op) {
        $inst = $op[0];

        // Should always be operation instances, but including verification.
        if ($inst instanceof OperationInterface) {
          $build['#items'][] = $inst->getTitle();
        }
      }

      $html = \Drupal::service('renderer')->renderRoot($build);
      $this->messenger()->addError($html);
    }
  }

}
