<?php

namespace Drupal\consume\Import;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Object for building a set of batch operations for running a single import.
 */
class ExclusiveBatchImporter extends BatchImporter implements BatchInterface, ExclusiveImporterInterface, ContainerInjectionStrategyInterface {

  use ExclusiveImporterTrait;

  /**
   * Create new exclusive batchable importer instance.
   *
   * @param string $id
   *   The importer strategy ID.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The importer definition.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The batch locking service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service for recording the batch process.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID generator.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime time service.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, LockBackendInterface $lock, StateInterface $state, UuidInterface $uuid, TimeInterface $time) {
    parent::__construct($id, $definition, $state, $uuid, $time);

    $this->lockService = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    return new static(
      $id,
      $definition,
      $container->get('lock.persistent'),
      $container->get('state'),
      $container->get('uuid'),
      $container->get('datetime.time')
    );
  }

}
