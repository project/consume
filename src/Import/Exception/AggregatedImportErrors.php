<?php

namespace Drupal\consume\Import\Exception;

/**
 * Creates an exception for reporting a series of importer mapping errors.
 */
class AggregatedImportErrors extends ResumeableException {

  /**
   * Array of string errors, captured from import data mapping.
   *
   * @var string[]
   */
  public $errors;

  /**
   * Create a new instance of this exception for throwing.
   */
  public function __construct(array $errors) {
    parent::__construct("Import errors:\r\n" . implode("\r\n - ", $errors));

    $this->errors = $errors;
  }

  /**
   * Get the array of errors.
   *
   * @return string[]
   *   An array of errors, keyed by the field from which the error originated.
   */
  public function getArrayErrors() {
    return $this->errors;
  }

}
