<?php

namespace Drupal\consume\Import\Exception;

/**
 * Exception thrown by parser when datasource is has an unrecoverable error.
 */
class InvalidDataSourceException extends \Exception {
}
