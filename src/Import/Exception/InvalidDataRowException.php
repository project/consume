<?php

namespace Drupal\consume\Import\Exception;

/**
 * Exception thrown by parser when a entry from the datasource is invalid.
 *
 * This means that the Parser was able to load the entry (can still determine
 * where the next record starts), but the data contained in that data entry
 * fails validation and cannot be imported.
 *
 * Example: A CSV row with the incorrect number of columns. This row
 * fails, but we can still read the next record after finding the newline.
 */
class InvalidDataRowException extends ResumeableException {
}
