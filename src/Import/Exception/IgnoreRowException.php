<?php

namespace Drupal\consume\Import\Exception;

/**
 * Exception to ignore a data row.
 *
 * This for skipping data entries without it being an error and when the row
 * should not be tracked. This is essentially ignoring that the data entry is
 * there and continuing with the rest of the processing.
 */
class IgnoreRowException extends \Exception {

  /**
   * The reason why this row was ignored.
   *
   * @var string
   */
  protected string $reason;

  /**
   * Create a new instance of the IgnoreRowException exception.
   *
   * @param string $reason
   *   The reason, if any additional information about the reason is available.
   * @param \Throwable|null $prev
   *   Any previous exception if the exception needs to be chained.
   */
  public function __construct(string $reason = 'ignore', ?\Throwable $prev = NULL) {
    parent::__construct(sprintf('Entry was ignored: %s', $reason), 0, $prev);

    $this->reason = $reason;
  }

  /**
   * Get the reason for why a importer might ignore this entry.
   *
   * @return string
   *   Get the reason why the item is being ignored.
   */
  public function getReason(): string {
    return $this->reason;
  }

}
