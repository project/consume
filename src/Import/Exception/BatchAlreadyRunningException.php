<?php

namespace Drupal\consume\Import\Exception;

/**
 * Error that the batch is already running and can't be started again.
 */
class BatchAlreadyRunningException extends \Exception {
}
