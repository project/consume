<?php

namespace Drupal\consume\Import\Exception;

/**
 * Exception to skip a data row.
 *
 * This differs from ResumeableException because the skipping of this entry
 * is not due to an error, and therefore does not stop even if "stop on error"
 * flag is set.
 *
 * ResumeableException can halt the import depending on error handling flags.
 */
class SkipRowException extends \Exception {

  const CHECKSUM = 'checksum';
  const NO_CHANGED = 'unchanged';

  /**
   * A reason code for the why this row was skipped.
   *
   * @var string
   */
  protected string $reason;

  /**
   * Should this row still be tracked even though it is being skipped.
   *
   * @var bool
   */
  protected bool $trackable;

  /**
   * Create a new instance of the SkipRowException exception.
   *
   * @param string $reason
   *   The reason, if any additional information about the reason is available.
   * @param bool $trackable
   *   Should this row be trackable, even though it was skipped?
   * @param \Throwable|null $prev
   *   Any previous exception if the exception needs to be chained.
   */
  public function __construct(string $reason = 'skip', bool $trackable = TRUE, ?\Throwable $prev = NULL) {
    parent::__construct(sprintf('Entry was skipped: %s', $reason), 0, $prev);

    $this->reason = $reason;
    $this->trackable = $trackable;
  }

  /**
   * Get the reason for why a importer might skip this entry.
   *
   * This can provide more information for import operations that need more
   * granularity. Reasons might include "checksum" for hashing checksum match
   * or "empty", etc...
   *
   * @return string
   *   Get the reason why the item is being skipped.
   */
  public function getReason(): string {
    return $this->reason;
  }

  /**
   * Even though this is being skipped, is this row still trackable.
   *
   * If the row is being skipped for a reason that the row is still valid
   * but didn't need to continue to be processed, the row should still be
   * tracked so a clean-up process is aware this entry is still valid. One
   * common example is that this was skipped because of "no change".
   *
   * Return FALSE if the values are invalid and should be removed.
   *
   * @return bool
   *   True if this row is still trackable. FALSE otherwise.
   */
  public function isTrackable(): bool {
    return $this->trackable;
  }

}
