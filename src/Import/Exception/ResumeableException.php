<?php

namespace Drupal\consume\Import\Exception;

/**
 * Exception for an import action which has errored but can be safely skipped.
 *
 * This exception indicates that an error was thrown, but an attempt to continue
 * the import process should be allowed if the import operation allows it.
 *
 * @see \Drupal\consume\Import\ImportOperationInterface::isResumeable()
 */
class ResumeableException extends \Exception {
}
