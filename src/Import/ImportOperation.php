<?php

namespace Drupal\consume\Import;

use Drupal\consume\ConsumerInterface;
use Drupal\consume\Import\Datasource\DatasourceInterface;
use Drupal\consume\Import\Exception\IgnoreRowException;
use Drupal\consume\Import\Exception\ResumeableException;
use Drupal\consume\Import\Exception\SkipRowException;
use Drupal\consume\Import\Persist\WriterInterface;
use Drupal\consume\Import\Tracker\HashingTrackerInterface;
use Drupal\consume\Import\Tracker\TrackerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * The default Consume import operation class.
 */
class ImportOperation implements ImportOperationInterface {

  use DependencySerializationTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * The number of items to run per batch process.
   *
   * The batch size to run for this operation. If 0, then the operation will
   * attempt to inherit the batch size from the owning Importer. The value is
   * "-1" (or less than 0), then the operation should run until all items are
   * completed.
   *
   * @var int
   */
  public int $batchSize = 0;

  /**
   * Should import exit on any data process, map or write error.
   *
   * Parsing errors are not captured and usually mean that the import will
   * not be able to continue. This only indicates that the batch can continue
   * with other rows of data if this entry fails. In most cases this might be
   * if data fails to validate.
   *
   * @var bool
   */
  public bool $exitOnError = FALSE;

  /**
   * The machine name of the batch operation.
   *
   * @var string
   */
  protected ?string $name;

  /**
   * A friendly label to display for the operation title.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup|string|null
   */
  protected $title;

  /**
   * Data mappers to massage the raw data into writeable format.
   *
   * @var \Drupal\consume\Import\Mapper\DataMapperInterface[]
   */
  protected array $mappers = [];

  /**
   * Data processors to alter the data, faciliate business logic or skip rows.
   *
   * @var \Drupal\consume\Import\Process\ProcessorInterface[]
   */
  protected array $processors = [];

  /**
   * The import tracker instance.
   *
   * @var \Drupal\consume\Import\Tracker\TrackerInterface
   */
  protected TrackerInterface $tracker;

  /**
   * The data writer instance.
   *
   * @var \Drupal\consume\Import\Persist\WriterInterface
   */
  protected WriterInterface $writer;

  /**
   * Creates a new instance of the ImportOperation class.
   *
   * @param \Drupal\consume\Import\Persist\WriterInterface $writer
   *   The import data writer. Persists the data to the target data destination.
   * @param \Drupal\consume\Import\Tracker\TrackerInterface $tracker
   *   The import data tracker.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $data_mappers
   *   The data property and field mappers.
   * @param \Drupal\consume\Import\Process\ProcessorInterface[] $processors
   *   The data row processors to transforming the datasets.
   */
  public function __construct(WriterInterface $writer, TrackerInterface $tracker, array $data_mappers, array $processors = []) {
    $this->tracker = $tracker;
    $this->writer = $writer;

    // Data manipulation and transformers.
    $this->mappers = $data_mappers;
    $this->processors = $processors;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->title ?? $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): self {
    $this->title = $title;
    return $this;
  }

  /**
   * Get the batch size defined for this import operation.
   *
   * If the value is 0, then operation should use the batch size of the parent
   * Importer instance. The value of -1 means unlimited, or don't batch.
   *
   * @return int
   *   The batch size preferred for this batch operation.
   */
  public function getBatchSize(): int {
    return $this->batchSize;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappers(): array {
    return $this->mappers;
  }

  /**
   * {@inheritdoc}
   */
  public function getTracker(): TrackerInterface {
    return $this->tracker;
  }

  /**
   * {@inheritdoc}
   */
  public function getWriter(): WriterInterface {
    return $this->writer;
  }

  /**
   * {@inheritdoc}
   */
  public function formatResult(array $result) {
    return $this->t('items processed @processed (@skipped skipped and @errors with errors).', [
      '@processed' => $result['itemsProcessed'] ?? 0,
      '@skipped' => $result['itemsSkipped'] ?? 0,
      '@errors' => $result['itemsError'] ?? 0,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function formatStatus(array $status): array {
    $properties = [
      'itemsProcessed' => $this->t('Items processed'),
      'itemsCreated' => $this->t('Items created'),
      'itemsUpdated' => $this->t('Items updated'),
      'itemsUnchanged' => $this->t('Items with no change'),
      'itemsSkipped' => $this->t('Items skipped'),
      'itemsError' => $this->t('Items with errrors'),
    ];

    $build = [];
    foreach ($properties as $key => $label) {
      $build[$key] = [
        '#theme' => 'consume_status_summary_field',
        '#label' => $label,
        '#value' => $status[$key] ?? '-',
      ];
    }

    $tracker = $this->getTracker();
    $hasHashing = ($tracker instanceof HashingTrackerInterface) && $tracker->allowHash();
    $build['itemUnchanged']['#access'] = $hasHashing || !empty($status['itemUnchanged']);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer): self {
    // Ensure that the data import writer has been initialized.
    $this->getTracker()->setup($importer, $this);
    $this->getWriter()->setup($importer, $this);

    foreach ($this->processors as $processor) {
      $processor->setup($importer, $this);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function init(DatasourceInterface $source, ImporterInterface $importer): self {
    $source->rewind();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function finalize(): self {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(ConsumerInterface $consumer, &$context = NULL): void {
    $opName = $this->getName();
    $importer = $consumer->getImporter();
    $source = $consumer->getDatasource($opName);

    if (!$source instanceof DatasourceInterface) {
      $error = sprintf('Invalid datasource provided for the "%s" operation.', $opName);
      throw new \InvalidArgumentException($error);
    }

    try {
      $consumer->ensureUserContext();
      $writer = $this->getWriter();

      if ($importer instanceof BatchInterface) {
        $limit = $this->getBatchSize() ?: $importer->getBatchSize();

        // Either resume progress or start at the beginning of the datasource.
        if (!empty($context['sandbox']['progress'])) {
          $source->resumeProgress($context['sandbox']['progress']);
        }
        else {
          $this->init($source, $importer);
          $context['results'][$opName]['itemsProcessed'] = 0;
          $context['results'][$opName]['itemsCreated'] = 0;
          $context['results'][$opName]['itemsUpdated'] = 0;
          $context['results'][$opName]['itemsUnchanged'] = 0;
          $context['results'][$opName]['itemsSkipped'] = 0;
          $context['results'][$opName]['itemsError'] = 0;
        }

        // Report the import batch operation to the calling context.
        $context['message'] = $this->t('Importing %data_source to %data_dest (%progress).', [
          '%data_source' => $source->getSourceName(),
          '%data_dest' => $writer->getDestinationName(),
          '%progress' => number_format($source->percentComplete() * 100, 1) . '%',
        ]);

        // Record our progress if batch has not completed yet. Next cycle will
        // pick-up from here.
        if ($this->doImport($consumer, $source, $context['results'][$opName], $limit)) {
          $this->finalize();
          $context['finished'] = 1;
        }
        else {
          $context['finished'] = max($source->percentComplete(), 0.01);
          $context['sandbox']['progress'] = $source->getProgress();
        }

        $importer->updateStatus($this, $context['results'][$opName]);
      }
      else {
        $results = [
          'itemsProcessed' => 0,
          'itemsCreated' => 0,
          'itemsUpdated' => 0,
          'itemsUnchanged' => 0,
          'itemsSkipped' => 0,
          'itemsError' => 0,
        ];

        $this->init($source, $importer);
        $this->doImport($consumer, $source, $results);
        $importer->updateStatus($this, $results);
        $this->finalize();
      }
    }
    catch (\Throwable $e) {
      $this->finalize();
      $consumer->finalize();

      $consumer->getErrorHandler()->handleThrowable($e);
      throw $e;
    }
    finally {
      // Ensure that the user context has been reset before exiting.
      $consumer->resetUserContext();
    }
  }

  /**
   * Perform the import operation.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer execution context for an importer process.
   * @param \Drupal\consume\Import\Datasource\DatasourceInterface $source
   *   The datasource to fetch data entries from.
   * @param array $results
   *   Reference to array with progress tracking data for this import operation.
   * @param int $limit
   *   The max number of items to process and import.
   *
   * @return bool
   *   Is the import operation complete?
   */
  protected function doImport(ConsumerInterface $consumer, DatasourceInterface $source, array &$results, int $limit = -1): bool {
    $mappers = $this->getMappers();
    $tracker = $this->getTracker();
    $writer = $this->getWriter();

    $count = 0;
    while ($data = $source->current()) {
      ++$count;
      ++$results['itemsProcessed'];
      $mappedData = [];

      try {
        // Transform and map the data in preparation to track and write
        // the data records.
        foreach ($this->processors as $processor) {
          $processor->preprocess($data, $mappers);
        }

        // Determine if there an existing record and update or create a new
        // data item with the updated data.
        $tracking = $tracker->fetchExisting($data, $mappers) ?? [];
        foreach ($this->processors as $processor) {
          $processor->processTracking($data, $mappers, $tracker, $tracking);
        }
        foreach ($mappers as $mapper) {
          $mapper->map($data, $mappedData, $mappers);
        }
        foreach ($this->processors as $processor) {
          $processor->process($data, $mappedData);
        }

        // Check if there is an existing import item, and update or create a
        // new one based on if it already existed or not.
        if ($item = $tracker->getItem($tracking)) {
          $item = $writer->updateEntry($mappedData, $item);
          ++$results['itemsUpdated'];
        }
        else {
          $item = $writer->createEntry($mappedData);
          ++$results['itemsCreated'];
        }

        $tracker->updateTracking($mappedData, $item, $tracking);
      }
      catch (IgnoreRowException $e) {
        // Just ignore this data entry.
      }
      catch (SkipRowException $e) {
        // Skip the processing of this row.
        if ($e->isTrackable()) {
          $tracker->refreshBatchId($data, $mappers);
        }

        $reason = $e->getReason();
        if (SkipRowException::CHECKSUM === $reason || SkipRowException::NO_CHANGED === $reason) {
          ++$results['itemsUnchanged'];
        }
        else {
          ++$results['itemsSkipped'];
        }
      }
      catch (ResumeableException $e) {
        if ($this->exitOnError) {
          throw $e;
        }

        // Exceptions that should be logged but are safe to proceed with.
        // Capturing them here will allow the data to still attempt to be
        // written.
        $consumer->getErrorHandler()->logThrowable($e);
        ++$results['itemsError'];
      }

      // Test for batch limit. Limit less than one, means unlimited.
      if ($limit > 0 && $count >= $limit) {
        break;
      }

      $source->next();
    }

    // Flush any buffered writes or tracking updates.
    $writer->flush();
    $tracker->flush();

    return $source->isComplete();
  }

}
