<?php

namespace Drupal\consume\Import;

use Drupal\consume\Import\Datasource\DatasourceInterface;
use Drupal\consume\Import\Persist\WriterInterface;
use Drupal\consume\Import\Tracker\TrackerInterface;

/**
 * Interface for defining a data import operation.
 */
interface ImportOperationInterface extends OperationInterface {

  /**
   * Get the data mappers for transforming the source data.
   *
   * Mappers are responsible for transforming the raw source data into the
   * expected format needed by the import writer.
   *
   * @return \Drupal\consume\Import\Mapper\DataMapperInterface[]
   *   An array of mappers which is used to transform the data from one
   *   representation to another.
   */
  public function getMappers(): array;

  /**
   * Gets the data tracker instance used with this import operation.
   *
   * @return \Drupal\consume\Import\Tracker\TrackerInterface
   *   The tracker instance used with this import operation.
   */
  public function getTracker(): TrackerInterface;

  /**
   * Gets the import data writer for operation.
   *
   * @return \Drupal\consume\Import\Persist\WriterInterface
   *   The import data writer for this operation.
   */
  public function getWriter(): WriterInterface;

  /**
   * Prepare the operation at the start of the operation run.
   *
   * For batched operations this is called on the first run of this batched
   * operation, when the sandbox is initialized for the batch context.
   *
   * @param \Drupal\consume\Import\Datasource\DatasourceInterface $source
   *   The datasource to fetch data from.
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The parent importer instance.
   *
   * @return self
   *   The operation instance for method chaining.
   */
  public function init(DatasourceInterface $source, ImporterInterface $importer): self;

}
