<?php

namespace Drupal\consume\Import;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\consume\ConsumerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The entity operation for cleaning up an entity from import.
 *
 * This is intended to clear stale entities that were previously imported but
 * do not appear during import file updates.
 *
 * Currently this operation only works with a single entity type, though in the
 * future we might be able to extend the ability to queue multiple entity types.
 */
class EntityCleanupOperation implements OperationInterface, ConfigurableInterface, ContainerInjectionInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  public const DEFAULT_CONFIGURATION = [
    'method' => 'unpublish',
  ];

  /**
   * The batch ID for import.
   *
   * @var string|null
   */
  protected ?string $batchId;

  /**
   * The operation identifier.
   *
   * @var string
   */
  protected ?string $name;

  /**
   * The operation display title.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup|string|null
   */
  protected $title;

  /**
   * The configuration for this clean-up operation.
   *
   * @var array
   */
  protected array $configuration;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type bundle information manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * The connection to the database to make select queries.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $db;

  /**
   * Creates a new instance of the EntityCleanupOperation class.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to create entity queries with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle information manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->db = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityBundleInfo = $entity_type_bundle_info;

    // Set the default configuration values.
    $this->configuration = static::DEFAULT_CONFIGURATION;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): self {
    $this->name = $name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->title ?: $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): self {
    $this->title = $title;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $config): self {
    $this->configuration = $config + static::DEFAULT_CONFIGURATION;
    return $this;
  }

  /**
   * Get a display label for the clean-up method performed byt this operation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The display value for the cleanup operation performed.
   */
  protected function getMethodLabel() {
    return $this->configuration['method'] ?? '';
  }

  /**
   * Gets the display name for the cleanup operation's target.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   The label to use for the operation cleanup target.
   */
  protected function getTargetLabel() {
    $config = $this->configuration;
    $entityTypeId = $config['entity_type'];
    $entityType = $this->entityTypeManager->getDefinition($entityTypeId);

    // Default to the entity type label.
    $typeLabel = $entityType->getLabel();

    // See if we can get more specific if there was a bundle filter.
    if (!empty($config['filters']['bundle'])) {
      $bundle = $config['filters']['bundle']['value'] ?? $config['filters']['bundle'];

      if (!is_array($bundle)) {
        $bundleInfo = $this->entityBundleInfo->getBundleInfo($entityTypeId);

        if (!empty($bundleInfo[$bundle])) {
          $typeLabel = $bundleInfo[$bundle]['label'];
        }
      }
    }

    return $typeLabel;
  }

  /**
   * {@inheritdoc}
   */
  public function formatResult(array $result) {
    return new FormattableMarkup('@count @type', [
      '@count' => $result['itemsCleaned'],
      '@type' => $this->getTargetLabel(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function formatStatus(array $status): array {
    return [
      '#theme' => 'consume_status_summary_field',
      '#label' => $this->t('@type_label items @method', [
        '@type_label' => $this->getTargetLabel(),
        '@method' => $this->getMethodLabel(),
      ]),
      '#value' => $status['itemsCleaned'] ?? 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer): self {
    if (empty($this->configuration['entity_type'])) {
      throw new \InvalidArgumentException('No entity types specified for the entity clean-up operation.');
    }

    // Ensure that the entity type exists and is available for querying.
    $entityType = $this->entityTypeManager->getDefinition($this->configuration['entity_type']);
    if (!($entityType instanceof ContentEntityTypeInterface || $entityType->getBaseTable())) {
      throw new \InvalidArgumentException('Entity clean-up operation is designed to work with content entity types with a SQL database table.');
    }

    $method = strtolower($this->configuration['method']) . 'Entities';
    if (!method_exists($this, $method)) {
      $err = sprintf('Entity clean-up operation does not have a method %s.', $this->configuration['method']);
      throw new \InvalidArgumentException($err);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function finalize(): self {
    return $this;
  }

  /**
   * Get the database table to use for the tracking information.
   *
   * @return string
   *   The name of the database table to build the tracking query from.
   */
  protected function getTrackingTable(): string {
    return $this->configuration['tracking_table'] ?? 'consume_tracker';
  }

  /**
   * The tracking name to utilize when when finding items to clean-up.
   *
   * The tracking name should match the tracker used for this import operation.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer being executed.
   *
   * @return string
   *   The name to use for the "import_name" in the tracking table.
   */
  protected function getTrackName(ImporterInterface $importer): string {
    // This value should match the tracker name used for the tracker of this
    // data. This allows importers with multiple operation stages to segment
    // multiple data types to remove and clear.
    return $this->configuration['track_name'] ?? $importer->id();
  }

  /**
   * Generate the query to find items which were not imported.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer to create the clean-up items for.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query to use to find entity items to clean up.
   */
  protected function getQuery(ImporterInterface $importer): AlterableInterface {
    $config = $this->getConfiguration();
    $entityType = $this->entityTypeManager->getDefinition($config['entity_type']);

    if ($entityTable = $entityType->getDataTable() ?: $entityType->getBaseTable()) {
      $query = $this->db->select($entityTable, 'entity');

      $idKey = $entityType->getKey('id');
      $query->leftJoin($this->getTrackingTable(), 'tracking', 'tracking.type=:entity_type AND tracking.import_name=:import_name AND tracking.item_id=entity.' . $idKey, [
        ':entity_type' => $entityType->id(),
        ':import_name' => $this->getTrackName($importer),
      ]);

      // Filters are currently only available for base field values.
      if (!empty($config['filters'])) {
        $fieldDefs = $this->entityFieldManager->getFieldStorageDefinitions($config['entity_type']);
        /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $entityStorage */
        $entityStorage = $this->entityTypeManager->getStorage($config['entity_type']);
        $tableMapping = $entityStorage->getTableMapping();

        $tables = [];
        foreach ($config['filters'] as $key => $filter) {
          // Convert to the entity key, if there is a matching one.
          if (in_array($key, ['status', 'bundle', 'published', 'label'])) {
            $key = 'entity.' . ($entityType->getKey($key) ?: $key);
          }
          else {
            $fieldKey = explode('.', $key, 2);

            if (isset($fieldDefs[$fieldKey[0]])) {
              $fieldTable = $tableMapping->getFieldTableName($fieldKey[0]);

              if (!isset($tables[$fieldTable])) {
                $tables[$fieldTable] = $fieldKey[0];
                $query->leftJoin($fieldTable, $key, "{$key}.entity_id = entity.{$idKey}");
              }

              $fieldDef = $fieldDefs[$fieldKey[0]];
              $propName = $fieldKey[1] ?? $fieldDef->getMainPropertyName();
              $key = "{$fieldKey[0]}." . $tableMapping->getFieldColumnName($fieldDef, $propName);
            }
          }

          $value = $filter['value'] ?? $filter;
          if (empty($filter['op'])) {
            $op = is_array($value) ? 'IN' : '=';
          }
          else {
            $op = $filter['op'];
          }

          $query->condition($key, $value, $op);
        }
      }

      $orCond = $query->orConditionGroup()
        ->condition('tracking.batch_id', $importer->getBatchId(), '!=')
        ->isNull('tracking.item_id');

      $query->condition($orCond);
      return $query;
    }

    // Should never happen since these conditions are checked in the
    // self::init() method, so it's unlikely we'd get this far.
    $err = sprintf('Unable to create an query for entity clean-up because no entity table is available for entity of type "%s".', $this->configuration['entity_type']);
    throw new \InvalidArgumentException($err);
  }

  /**
   * Prepare the operation before it is initially run.
   *
   * This is called once at the start of the operation processing to ensure
   * a consistent initial state before running.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer which is being run.
   *
   * @return self
   *   The operation itself for method chaining.
   */
  public function init(ImporterInterface $importer): self {
    $delQuery = $this->db->delete($this->getTrackingTable());
    $delQuery
      ->condition('import_name', $this->getTrackName($importer))
      ->condition('type', $this->configuration['entity_type'])
      ->condition('batch_id', $importer->getBatchId(), '!=')
      ->execute();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(ConsumerInterface $consumer, &$context = NULL): void {
    $importer = $consumer->getImporter();

    try {
      $entityTypeId = $this->configuration['entity_type'];
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
      $idKey = $entityType->getKey('id');

      // Should be checked during the operation initialization.
      $method = strtolower($this->configuration['method']) . 'Entities';
      if (!method_exists($this, $method)) {
        $err = sprintf('Entity clean-up operation does not have a method %s.', $this->configuration['method']);
        throw new \InvalidArgumentException($err);
      }

      $consumer->ensureUserContext();

      /** @var \Drupal\Core\Database\Query\SelectInterface $query */
      $query = $this->getQuery($importer);
      if ($importer instanceof BatchInterface) {
        if (!isset($context['sandbox']['progress'])) {
          $counter = clone $query;
          $context['sandbox']['progress'] = 0;
          $context['sandbox']['current'] = 0;
          $context['sandbox']['total'] = $counter->countQuery()->execute()->fetchField();

          // Run the batch initialization functions.
          $this->init($importer);
        }

        $ids = $query
          ->fields('entity', [$idKey])
          ->condition("entity.{$idKey}", $context['sandbox']['current'], '>')
          ->orderBy("entity.{$idKey}", 'ASC')
          ->range(0, $importer->getBatchSize())
          ->execute()
          ->fetchCol();

        if ($ids) {
          $entities = $entityStorage->loadMultiple($ids);
          $this->{$method}($entities);
          $context['sandbox']['progress'] += count($ids);
          $context['sandbox']['current'] = end($ids);

          if ($context['sandbox']['progress'] < $context['sandbox']['total']) {
            $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['total'];
          }
        }

        // Capture the status of the items being cleaned.
        $context['results'][$this->getName()]['itemsCleaned'] = $context['sandbox']['progress'];
        $importer->updateStatus($this, ['itemsCleaned' => $context['sandbox']['progress']]);
      }
      else {
        $this->init($importer);
        $ids = $query
          ->fields('entity', [$idKey])
          ->execute()
          ->fetchCol();

        if ($ids) {
          $entities = $entityStorage->loadMultiple($ids);
          $this->{$method}($entities);
        }

        // Capture the status of number of items being cleaned.
        $importer->updateStatus($this, ['itemsCleaned' => count($ids)]);
      }
    }
    catch (\Throwable $e) {
      $importer->finalize();
      throw $e;
    }
    finally {
      // Ensure that the user context has been reset before exiting.
      $consumer->resetUserContext();
    }

  }

  /**
   * Unpublish list of entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The entities to unpublish.
   */
  protected function unpublishEntities(array $entities): void {
    $entity = reset($entities);

    if ($entity instanceof EntityPublishedInterface) {
      /** @var \Drupal\Core\Entity\EntityPublishedInterface[] $entities */
      foreach ($entities as $entity) {
        $entity->setUnpublished()->save();
      }
    }
    else {
      $entityType = $entity->getEntityType();
      $statusKey = $entityType->getKey('published') ?: $entityType->getKey('status');

      if (empty($statusKey)) {
        $err = sprintf('Unable to unpublish entity of type %s, not able to determine the publishing flag.', $entityType->getLabel());
        throw new \InvalidArgumentException($err);
      }
      foreach ($entities as $entity) {
        $entity->set($statusKey, FALSE)->save();
      }
    }
  }

  /**
   * Delete the list of entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   List of entities to delete.
   */
  protected function deleteEntities(array $entities): void {
    $this->entityTypeManager
      ->getStorage($this->configuration['entity_type'])
      ->delete($entities);
  }

}
