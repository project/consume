<?php

namespace Drupal\consume\Import\Tracker;

/**
 * Implements a tracker instance that has storage for a checksum hash.
 *
 * The checksum hash is normally used in combination with the "HashingProcessor"
 * (or a variant of), which does the hashing and comparison. The tracker only
 * manages the storage and fetching of the hash value.
 */
interface HashingTrackerInterface extends TrackerInterface {

  /**
   * Does this tracker support storing and using a checksum / hash value.
   *
   * @return bool
   *   TRUE if the tracker supports storage of a checksum hash. FALSE otherwise.
   */
  public function allowHash(): bool;

  /**
   * If this tracker supports hash checksums, clear the hash values.
   *
   * If the tracker allows for tracking of hashing, clear the hash values.
   * This allows all records to update regardless of if the source data was
   * changed.
   */
  public function clearHash(): void;

}
