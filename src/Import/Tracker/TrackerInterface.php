<?php

namespace Drupal\consume\Import\Tracker;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;

/**
 * Interface for objects responsible for tracking imported data.
 *
 * Trackers keep track of what data has been imported and what they related
 * to with internal Drupal data. This allows the system to find existing
 * content for updates and avoiding creating duplicates. It also allows the
 * system to create clean-up operations based on the tracking information.
 */
interface TrackerInterface extends PluginInspectionInterface {

  /**
   * Initiate tracking information at the start of an import process.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer to which the import operation belongs.
   * @param \Drupal\consume\Import\ImportOperationInterface $operation
   *   The import operation that utilizes this writer instance.
   *
   * @return self
   *   The tracker instance for method chaining.
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self;

  /**
   * Get the ID of the batch currently being run.
   *
   * @return string
   *   ID for the current batch that is being run.
   */
  public function getBatchId(): string;

  /**
   * Set the batch ID naming.
   *
   * @param string $batch_id
   *   The batch identifier to set for the tracker instance.
   *
   * @return self
   *   The tracker instance for method chaining.
   */
  public function setBatchId(string $batch_id): self;

  /**
   * Get an ID for indicating which data entry from the feed is being processed.
   *
   * @param array $data
   *   The data from the feed to derive the ID information from.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   Import operation mappers.
   *
   * @return mixed
   *   Get an data identifier for this data entry.
   */
  public function getDataId(array $data, array $mappers = []);

  /**
   * Get the tracked item's type identifier.
   *
   * This helps to identify the target item being tracked, and allows importers
   * to support multiple types of items and avoid ID collisions. An example
   * would be the entity type for tracking entities.
   *
   * @param mixed $object
   *   The tracked data item to compute an identifier for.
   *
   * @return string
   *   The item type.
   */
  public function getItemType($object): string;

  /**
   * Gets a identifier for the tracked object.
   *
   * @param mixed $object
   *   The tracked data item to compute an identifier for.
   *
   * @return string
   *   An identifier to be used for this object in the tracking table.
   */
  public function getItemId($object): string;

  /**
   * Extracts the target item from the tracking info.
   *
   * Allows the tracker to have an internal storage for item within its
   * tracking data.
   *
   * @param array $info
   *   The tracking information fetched by the tracker previously.
   *
   * @return mixed|null
   *   The tracked target item in the tracking information. NULL if
   */
  #[\ReturnTypeWillChange]
  public function getItem(array &$info);

  /**
   * Based on the data, fetch an existing object if a match is found.
   *
   * @param array $data
   *   Raw data to find imported data from.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   Import operation mappers.
   *
   * @return array|null
   *   Return resulting tracking information if an existing entry already
   *   exists. Returns NULL if there is no matching data.
   */
  public function fetchExisting(array $data, array $mappers = []): ?array;

  /**
   * Update the batch ID and date for a tracking entry.
   *
   * Refreshes just the batch ID for the tracking so the system is aware that
   * the entry was seen during an import. Typically this is used when the entry
   * is being skipped, or doesn't need to be updated.
   *
   * @param array $data
   *   The input data to determine how the entry is being tracked.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   Import operation mappers.
   */
  public function refreshBatchId(array $data, array $mappers = []): void;

  /**
   * Update the tracking information on the data being committed.
   *
   * @param array $data
   *   The raw data being written.
   * @param mixed $item
   *   The target written object after the writer has persisted it.
   * @param array $tracking
   *   Existing tracking information to reference for updating the tracking.
   */
  public function updateTracking(array $data, $item, array $tracking = []): void;

  /**
   * Delete a group of items from the tracking.
   *
   * @param array $ids
   *   An array of IDs to indicate which items to remove.
   */
  public function delete(array $ids): void;

  /**
   * Execute tasks at the end of a batch or group of operations.
   *
   * For efficiency, some operations may get queued to run in a batch. This
   * method persists any outstanding items that have been queued. Ensure
   * that this is run before exiting batch operations.
   */
  public function flush(): void;

}
