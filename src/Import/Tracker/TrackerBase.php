<?php

namespace Drupal\consume\Import\Tracker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\consume\Import\BatchInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create an import tracker that just creates a batch ID.
 */
abstract class TrackerBase extends PluginBase implements HashingTrackerInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * Identifier of this batched operation to use with progress tracking.
   *
   * @var string
   */
  protected $batchId;

  /**
   * Get the name of the import to track data for.
   *
   * @var string
   */
  protected $importName;

  /**
   * Time service for getting request, or system time consistently.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Connection to the database for recording the tracker data.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * Creates a new instance of a TrackerInterface implementation.
   *
   * @param array $configuration
   *   The pluign configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Database\Connection $db
   *   Connection to the default database to execute tracking queries on.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime - time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $db, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->db = $db;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self {
    if ($importer instanceof BatchInterface) {
      $this->setBatchId($importer->getBatchId());
    }

    $this->importName = $this->configuration['track_name'] ?? $importer->id();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataId(array $data, array $mappers = []) {
    $dataCol = $this->configuration['id'];
    return $data[$dataCol];
  }

  /**
   * {@inheritdoc}
   */
  public function allowHash(): bool {
    return $this->configuration['use_hash'] ?? TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getBatchId(): string {
    return $this->batchId;
  }

  /**
   * {@inheritdoc}
   */
  public function setBatchId(string $batch_id): self {
    $this->batchId = $batch_id;
    return $this;
  }

  /**
   * Gets database table to write the import tracking data to.
   *
   * @return string
   *   The database table to write the import tracking data to.
   */
  public function getTable(): string {
    return $this->configuration['track_table'] ?? 'consume_tracker';
  }

  /**
   * {@inheritdoc}
   */
  public function refreshBatchId(array $data, array $mappers = []): void {
    if (empty($this->configuration['no_track'])) {
      $this->db->update($this->getTable())
        ->fields([
          'batch_id' => $this->batchId,
          'date' => $this->time->getRequestTime(),
        ])
        ->condition('import_name', $this->importName)
        ->condition('id', $this->getDataId($data, $mappers))
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateTracking(array $data, $item, array $tracking = []): void {
    if (empty($this->configuration['no_track'])) {
      $this->db->merge($this->getTable())
        ->keys([
          'import_name' => $this->importName,
          'id' => $this->getDataId($data),
        ])
        ->fields([
          'batch_id' => $this->batchId,
          'type' => $this->getItemType($item),
          'item_id' => $this->getItemId($item),
          'hash' => $tracking['hash'] ?? NULL,
          'date' => $this->time->getRequestTime(),
        ])
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearHash(): void {
    $this->db->update($this->getTable())
      ->fields(['hash' => NULL])
      ->condition('import_name', $this->importName)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $ids): void {
    // @todo Find a good way to map these data IDs back to the tracking table
    // and clear related items.
  }

  /**
   * {@inheritdoc}
   */
  public function flush(): void {
    // Does all it's work immediately, and doesn't need to do anything here.
  }

}
