<?php

namespace Drupal\consume\Import;

use Drupal\consume\ConsumerInterface;
use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Interface for defining a data import operation.
 */
interface ImporterInterface extends StrategyInterface {

  /**
   * Get an identifier for the import operation being run with this importer.
   *
   * Even importers which do not implement the BatchInterface should have this
   * ID to differentiate one process run from another. Consider them a single
   * context batch run.
   *
   * @return string
   *   The ID of the batch run currently being run.
   */
  public function getBatchId(): string;

  /**
   * Get friendly importer title.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface|string
   *   Translatable batch title.
   */
  public function getTitle();

  /**
   * Adds an import operation.
   *
   * @param string $name
   *   The machine name for this operation.
   * @param \Drupal\consume\Import\OperationInterface $operation
   *   The import operation to add.
   *
   * @return self
   *   The importer instance for method chaining.
   */
  public function addOperation(string $name, OperationInterface $operation): self;

  /**
   * Get an array of sequential import operations.
   *
   * @return \Drupal\consume\Import\OperationInterface[]
   *   An array of operations instances to perform the the import operation.
   */
  public function getOperations(): array;

  /**
   * Gets the stored import status info from the last time the importer ran.
   *
   * @return array
   *   Get the stored status information from the latest process run.
   */
  public function getStatusInfo(): array;

  /**
   * Update the status of importer operations.
   *
   * Allows operations provide information and notification of their completion.
   * The importer should record the status information for the operation and
   * update the last completed phase / operation information.
   *
   * @param \Drupal\consume\Import\OperationInterface $operation
   *   The operation that has completed and is reporting status data.
   * @param mixed $status
   *   The status information from the operation to report the status of.
   */
  public function updateStatus(OperationInterface $operation, $status): void;

  /**
   * Initialize the batch. Typically setup trackers and prepare data writers.
   *
   * This is initialization that should occur before a batch is started. This
   * initialization should rely on the datasource being setup yet.
   */
  public function setup(): void;

  /**
   * Executes the importer and runs all the operations.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer executable context for the import operations.
   * @param array|null $op_names
   *   The importer operations to run, if NULL or empty, all available
   *   operations will be run.
   */
  public function execute(ConsumerInterface $consumer, ?array $op_names = NULL): void;

  /**
   * Import has finished or errored, clean-up before exiting.
   */
  public function finalize(): void;

}
