<?php

namespace Drupal\consume\Import;

use Drupal\consume\ConsumerInterface;

/**
 * Interface for defining a data import operation.
 */
interface OperationInterface {

  /**
   * Get the machine name of the import operation.
   *
   * @return string
   *   Get a name that identifies this importer.
   */
  public function getName(): string;

  /**
   * The machine name of the operation in the importer.
   *
   * @param string $name
   *   The operation machine name to identify this operation in the importer.
   *
   * @return self
   *   The operation itself for method chaining.
   */
  public function setName(string $name): self;

  /**
   * A human friendly display name for this batch operation.
   *
   * @return string
   *   The human friendly title for this batch operation.
   */
  public function getTitle(): string;

  /**
   * Sets the title for operations.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $title
   *   The human friendly label to use for this import operation.
   *
   * @return self
   *   The operation itself for method chaining.
   */
  public function setTitle($title): self;

  /**
   * Format the operation processing results into a displayable markup format.
   *
   * @param array $result
   *   Array of operation result data created during the operation execution.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Displayable markup for the operation processing results.
   */
  public function formatResult(array $result);

  /**
   * Format the status information into a renderable.
   *
   * @param array $status
   *   Status information from the last batch process execution. The content is
   *   specific to the operation.
   *
   * @return array
   *   A renderable array to display the status information for this operation.
   */
  public function formatStatus(array $status): array;

  /**
   * Setup and prepare the batch (prepare trackers and data writers).
   *
   * This is setup occurs outside of the batch processing and is called before
   * the batch is started. This initialization should not rely on datasources
   * of the state of the operation being ready.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The parent importer instance.
   *
   * @return self
   *   The operation instance for method chaining.
   */
  public function setup(ImporterInterface $importer): self;

  /**
   * Run the operation.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer which consists of the importer and execution context.
   * @param array|null $context
   *   Reference to the batch context if this is a batch operation.
   */
  public function __invoke(ConsumerInterface $consumer, &$context = NULL): void;

  /**
   * Import operation clean-up processing before exit.
   *
   * Finalize can be called for successful completion or for an error. The
   * finalize method to ensure that held locks or resources are released.
   *
   * @return self
   *   The operation instance for method chaining.
   */
  public function finalize(): self;

}
