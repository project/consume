<?php

namespace Drupal\consume\Import;

use Drupal\consume\ConsumeTrackerPluginManager;
use Drupal\consume\ConsumeWriterPluginManager;
use Drupal\consume\Import\Mapper\ValueMapper;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Factory to build import operations from definition files.
 */
class ImportOperationFactory {

  /**
   * Factory for creating class instances from definitions.
   *
   * Handles dependency injection for classes that implement
   * \Drupal\Core\DependencyInjection\ContainerInjectionInterface.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * The import tracker plugin manager.
   *
   * @var \Drupal\consume\ConsumeTrackerPluginManager
   */
  protected ConsumeTrackerPluginManager $trackerManager;

  /**
   * The import writer plugin manager.
   *
   * @var \Drupal\consume\ConsumeWriterPluginManager
   */
  protected ConsumeWriterPluginManager $writerManager;

  /**
   * Creates a new instance of the ImportOperationFactory class.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver instance.
   * @param \Drupal\consume\ConsumeTrackerPluginManager $tracker_manager
   *   The import tracker plugin manager.
   * @param \Drupal\consume\ConsumeWriterPluginManager $writer_manager
   *   The import writer plugin manager.
   */
  public function __construct(ClassResolverInterface $class_resolver, ConsumeTrackerPluginManager $tracker_manager, ConsumeWriterPluginManager $writer_manager) {
    $this->classResolver = $class_resolver;
    $this->trackerManager = $tracker_manager;
    $this->writerManager = $writer_manager;
  }

  /**
   * Builds and prepares a import operation.
   *
   * @param string $name
   *   The operation ID and machine name.
   * @param array $definition
   *   The import operation definition. This will contain configurations and
   *   settings for for how to build the operation, and its component instances.
   *
   * @return \Drupal\consume\Import\ImportOperationInterface
   *   A built import operation, which matches the operation definition.
   */
  public function createOperation(string $name, array $definition): OperationInterface {
    $definition += ['class' => ImportOperation::class];
    $opClass = $definition['class'];

    // If a full import operation then build the handlers and use them to
    // populate the operation.
    if (is_subclass_of($opClass, ImportOperationInterface::class)) {
      if (empty($definition['writer'])) {
        $err = sprintf('Unable to create operation: "%s" without a data destination.', $name);
        throw new \InvalidArgumentException($err);
      }

      // Tracker definition has a default NULL tracker if no tracking is needed.
      $trackerDef = $definition['tracker'] ?? ['plugin' => 'null_tracker'];
      $tracker = $this->trackerManager->createInstance($trackerDef['plugin'], $trackerDef['configuration'] ?? []);

      $writerDef = $definition['writer'];
      $writer = $this->writerManager->createInstance($writerDef['plugin'], $writerDef['configuration'] ?? []);

      $mappers = $this->createMappers($definition['mappers'] ?? []);
      $processors = $this->createProcessors($definition['processors'] ?? []);

      $operation = new $opClass($writer, $tracker, $mappers, $processors);
    }
    else {
      $operation = $this->createInstance($definition);
    }

    // If a title is available, set the operation value to be more friendly.
    if (!empty($definition['title'])) {
      $operation->setTitle($definition['title']);
    }
    $operation->setName($name);
    return $operation;
  }

  /**
   * Creates an instance based on the $definition and class information.
   *
   * @param array $definition
   *   The import handler definition. Should at a minimum have a "class" value
   *   which is the fully namespaced class name.
   *
   * @return object
   *   An instance of the import handler built from the definition information.
   */
  protected function createInstance(array $definition): object {
    if (!isset($definition['class'])) {
      throw new \InvalidArgumentException('Importer operation definition is missing a "class" which is required.');
    }
    if (!class_exists($definition['class'])) {
      $error = sprintf('Importer operation definition has class "%s" which does not exist.', $definition['class']);
      throw new \InvalidArgumentException($error);
    }

    $classType = $definition['class'];
    if (is_subclass_of($classType, ContainerInjectionInterface::class)) {
      $instance = $this->classResolver->getInstanceFromDefinition($classType);

      // Apply configurations to the new class instance if any were provided.
      if ($instance instanceof ConfigurableInterface) {
        if (!empty($definition['arguments'])) {
          $instance->setConfiguration($definition['arguments']);
        }
        elseif (!empty($definition['configuration'])) {
          $instance->setConfiguration($definition['configuration']);
        }
      }
      return $instance;
    }
    else {
      $reflected = new \ReflectionClass($classType);

      // Apply constructor arguments directly if they were provided.
      // With PHP 8.0, be weary of named parameter passing.
      return isset($definition['arguments'])
        ? $reflected->newInstanceArgs($definition['arguments'])
        : $reflected->newInstance();
    }
  }

  /**
   * The data mappers generated form the mapper definitions.
   *
   * @param array $definitions
   *   Array of mapper definitions to build.
   *
   * @return \Drupal\consume\Import\Mapper\DataMapperInterface[]
   *   Array of generated mapper isntances, keyed by their data destination
   *   name ID.
   */
  public function createMappers(array $definitions): array {
    $mappers = [];

    foreach ($definitions as $dst => $definition) {
      if (is_string($definition)) {
        $definition = [
          'class' => ValueMapper::class,
          'arguments' => [$definition],
        ];
      }

      $definition += ['arguments' => []];
      array_unshift($definition['arguments'], $dst);
      $mappers[$dst] = $this->createInstance($definition);
    }

    return $mappers;
  }

  /**
   * Creates import processors from their definitions.
   *
   * @param array $definitions
   *   The processor definitions to build.
   *
   * @return \Drupal\consume\Import\Process\ProcessorInterface[]
   *   An array of processors
   *
   * @throws \InvalidArgumentException
   *   A processor class is not available or missing.
   */
  public function createProcessors(array $definitions): array {
    $processors = [];

    foreach ($definitions as $id => $definition) {
      if (is_string($definition)) {
        $definition = ['class' => $definition];
      }

      $processors[$id] = $this->createInstance($definition);
    }

    return $processors;
  }

}
