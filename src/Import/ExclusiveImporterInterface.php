<?php

namespace Drupal\consume\Import;

/**
 * Interface for importer that cannot have multiple instances in parallel.
 */
interface ExclusiveImporterInterface extends ImporterInterface {

  /**
   * Get the name of the import, used for identifying the lock name.
   *
   * @return string
   *   The batch identifier to lock on.
   */
  public function getLockId(): string;

  /**
   * Check the status of the importer lock.
   *
   * @return bool
   *   Is the lock available for acquiring.
   */
  public function isLockAvailable(): bool;

  /**
   * Acquire a lock for this import operation.
   *
   * This allows an import to know if another instance is already running
   * and prevents multiple batch processes from running at the same time. Could
   * potentially cause issue if the tracking table is updated inconsistently.
   *
   * @throws \Drupal\consume\Import\Exception\BatchAlreadyRunningException
   *   If unable to obtain a persistent batch for this import instance. This
   *   means it is likely the batch is already running.
   */
  public function acquireLock(): void;

  /**
   * Release the lock for this importer instance.
   */
  public function releaseLock(): void;

}
