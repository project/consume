<?php

namespace Drupal\consume\Import\Error;

use Drupal\consume\ConsumerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;

/**
 * Error handler which logs the errors to the Drupal watchdog or system log.
 */
class LogErrorHandler implements ErrorHandlerInterface {

  use LoggerChannelTrait;

  /**
   * The consumer execution context that this error handler is for.
   *
   * @var \Drupal\consume\ConsumerInterface
   */
  protected ConsumerInterface $consumer;

  /**
   * Create a new instance of the logging consumer error handler.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer execution context to handle the logging for.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface|null $logger_factory
   *   The logger channel factory if available (fetches using trait if NULL).
   */
  public function __construct(ConsumerInterface $consumer, LoggerChannelFactoryInterface $logger_factory = NULL) {
    $this->consumer = $consumer;

    if ($logger_factory) {
      $this->setLoggerFactory($logger_factory);
    }
  }

  /**
   * Get the channel to log to.
   *
   * @return string
   *   Get the logger channel to log to.
   */
  protected function getLogChannel(): string {
    $importerId = $this->consumer->getImporter()->id();
    return 'consume.' . $importerId;
  }

  /**
   * {@inheritdoc}
   */
  public function handleThrowable(\Throwable $error): void {
    $this->logThrowable($error);
  }

  /**
   * {@inheritdoc}
   */
  public function logThrowable(\Throwable $error): void {
    $this->log(RfcLogLevel::ERROR, Error::renderExceptionSafe($error));
  }

  /**
   * {@inheritdoc}
   */
  public function log($severity, $message, array $context = []): void {
    $this
      ->getLogger($this->getLogChannel())
      ->log($severity, $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function finalize(): void {
    // All the errors already handled as they are encountered.
  }

}
