<?php

namespace Drupal\consume\Import\Error;

/**
 * Interface for implementing error handlers for consume importers.
 *
 * Creating an error handler and assigning it to a Consumer execution context
 * allows flexibility in how to handle errors for different use cases. One
 * example would be to have a error handler that sends a notification for a
 * nightly batch execution (no user present), but use a handler to just display
 * the error when run from an admin form (user is present).
 */
interface ErrorHandlerInterface {

  /**
   * Handle the error.
   *
   * Perform error handling on errors which are ending the running process. This
   * is an opportunity for us to run some system tasks before exiting the
   * process.
   *
   * The exception / throwable is expected to be rethrown after being handled,
   * so the running process will full exit.
   *
   * @param \Throwable|\Exception $error
   *   Error to handle.
   */
  public function handleThrowable(\Throwable $error): void;

  /**
   * Capture the error in the system logs.
   *
   * This just logs the error and exception. Can be part of the method
   * static::handleThrowable() to consistently log the error, or for errors
   * that do not fully exit the process (safe to resume errors).
   *
   * @param \Throwable|\Exception $error
   *   The error to log.
   */
  public function logThrowable(\Throwable $error): void;

  /**
   * Log messages.
   *
   * @param string $severity
   *   The level or severity of the message being logged. This should match to
   *   PSR LogLevel logging severity values (info, status, error, etc...).
   * @param string|\Stringable $message
   *   The message to log.
   */
  public function log($severity, $message): void;

  /**
   * What batch exits, give error handler a chance to complete its work.
   *
   * This allows an error handler to accumulate errors and report them all at
   * once when the batch ends. An example of this is the EmailNotifyErrorHandler
   * which tracks resumeable errors and sends a single email notification with
   * all the buffered errors.
   */
  public function finalize(): void;

}
