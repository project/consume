<?php

namespace Drupal\consume\Import\Error;

use Drupal\consume\ConsumerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Consumer ErrorHandler for sending email notifications on error.
 */
class EmailNotifyErrorHandler extends LogErrorHandler {

  /**
   * The email address or addresses where the notification will be sent to.
   *
   * The formatting of this string will be validated with the
   * http://php.net/manual/filter.filters.validate.php.
   *
   * @var string
   * @link http://php.net/manual/filter.filters.validate.php PHP email validation filter. @endlink
   */
  protected string $recipients;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface|null
   */
  protected ?LanguageManagerInterface $languageManager;

  /**
   * Buffer of errors that should be sent in the notification.
   *
   * @var array
   */
  protected array $notifyBuffer = [];

  /**
   * Create a new instance of the EmailNotifyErrorHandler class.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer execution context that the handler is attached to.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface|null $language_manager
   *   The language manager.
   */
  public function __construct(ConsumerInterface $consumer, MailManagerInterface $mail_manager, LanguageManagerInterface $language_manager = NULL) {
    parent::__construct($consumer);

    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Set the language manager to use with this error handler.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service to use with this error handler.
   *
   * @return self
   *   Returns the error handler for method chaining.
   */
  public function setLanguageManager(LanguageManagerInterface $language_manager): self {
    $this->languageManager = $language_manager;
    return $this;
  }

  /**
   * Get the recipients to send the error notification to.
   *
   * @return string|null
   *   The email address or addresses where the notification will be sent to.
   */
  public function getRecipients(): ?string {
    return $this->recipients;
  }

  /**
   * Set the recipient to send email notifications to when an error is detected.
   *
   * @param string|null $notify_recipients
   *   The email recipients to send the error notification emails to.
   *
   * @return self
   *   Returns the error handler for method chaining.
   */
  public function setRecipients(?string $notify_recipients): self {
    $this->recipients = $notify_recipients;
    return $this;
  }

  /**
   * Get the default message language code.
   *
   * Currently get the current language and uses that.
   *
   * @todo Take into account the recipients when determining the language?
   *
   * @return string
   *   The default language to send the notification email with.
   */
  protected function getDefaultLanguage(): string {
    $langManager = $this->languageManager ?: \Drupal::languageManager();
    return $langManager->getCurrentLanguage()->getId();
  }

  /**
   * Create the email subject for reporting errors.
   *
   * @return string
   *   The email subject for the error notification.
   */
  protected function buildSubject(): string {
    $importer = $this->consumer->getImporter();
    return sprintf('Importing error for: %s', $importer->getTitle());
  }

  /**
   * Build the email body content for the notification.
   *
   * @return array
   *   The email body content to report the error.
   */
  protected function buildMessage(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function handleThrowable(\Throwable $error): void {
    $this->logThrowable($error);
  }

  /**
   * The processing has been completed, run any finish up tasks.
   *
   * In the case of the email notify error handler, we gather up all the errors
   * and build the error information into an email and send it.
   */
  public function finalize(): void {
    $recipients = $this->getRecipients();

    // Send an email notification if there are recipients setup for it.
    if (!empty($recipients)) {
      $importId = $this->consumer->getImporter()->id();
      $langCode = $this->getDefaultLanguage();

      $this->mailManager->mail('consume', 'error_notify:' . $importId, $recipients, $langCode, [
        'subject' => $this->buildSubject(),
        'body' => $this->buildMessage(),
      ]);
    }
  }

}
