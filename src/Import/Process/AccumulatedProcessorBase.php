<?php

namespace Drupal\consume\Import\Process;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\consume\Import\ConfigurableInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add data from an storage accumulator.
 *
 * In order to support data from multiple datasources (e.g. multiple files),
 * we can store the data in storage accumulators (an example would be the
 * Writer plugin "data_accumulator"). This processor applies data accumulated
 * into a database table.
 *
 * This base class does not apply the data, and should be subclassed to apply
 * the stored data as appropriate.
 *
 * @todo Add data merging options (i.e. Overwrite, merge, add only, etc...).
 *
 * @see \Drupal\consume\Plugin\Consume\Writer\DataAccumulator
 */
abstract class AccumulatedProcessorBase extends ProcessorBase implements ProcessorInterface, ConfigurableInterface, ContainerInjectionInterface {

  use DependencySerializationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * The data serializer.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * The configuration which should indicate the table and ID of the data field.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Creates a new instance of the ApplyAccumulatedProcessor data processor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database to fetch stored data from.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The data serializer to use when decoding data from the storage.
   */
  public function __construct(Connection $database, SerializationInterface $serializer) {
    $this->db = $database;
    $this->serializer = $serializer;

    // Ensure configuration defaults are at least available.
    $this->setConfiguration([]);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('serialization.json')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): self {
    $this->configuration = $configuration + [
      'table' => $this->configuration['table'] ?? 'consume_data_accumulator',
    ];

    return $this;
  }

  /**
   * Get the stored data value for a identifier.
   *
   * @param string $valueId
   *   The ID to find a matching data storage values.
   *
   * @return array
   *   The matching data storage value. An empty array is returned if no
   *   stored value is available.
   */
  protected function fetchValues(string $valueId): array {
    $table = $this->configuration['table'];
    $importId = $this->configuration['import_name'] ?? $this->importId;

    // Check if there is an accumulated data to add.
    $values = $this->db->select($table, 'data')
      ->fields('data', ['data'])
      ->condition('import_name', $importId)
      ->condition('id', $valueId)
      ->execute()
      ->fetchField();

    return $this->serializer->decode($values) ?? [];
  }

}
