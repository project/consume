<?php

namespace Drupal\consume\Import\Process;

use Drupal\consume\Import\ConfigurableInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Add data from an storage accumulator to mapped data before writing.
 *
 * In order to support data from multiple datasources (e.g. multiple files),
 * we can store the data in storage accumulators (an example would be the
 * Writer plugin "data_accumulator"). This processor applies data accumulated
 * into a database table.
 *
 * The processor adds the accumulated data to the mapped data in the ::process()
 * method.
 *
 * @see \Drupal\consume\Plugin\Consume\Writer\DataAccumulator
 */
class ApplyAccumulatedProcessor extends AccumulatedProcessorBase implements ProcessorInterface, ConfigurableInterface, ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function process(array $data, array &$mappedData): void {
    $idField = $this->configuration['id'];

    if (!empty($data[$idField])) {
      $mappedData += $this->fetchValues($mappedData[$idField]);
    }
  }

}
