<?php

namespace Drupal\consume\Import\Process;

use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\OperationInterface;
use Drupal\consume\Import\Tracker\TrackerInterface;

/**
 * Interface for data import processors.
 *
 * Processors are use to transform the raw data before the data mappers are run
 * and again after data has been mapped. Processors are meant to look at the
 * data entry more holistically as complete set and apply business logic,
 * filtering, or determine if the row should be skipped; where as, data mappers
 * are more focused on data translation of properties and fields.
 *
 * Processors that have configurations and settings should use the
 * \Drupal\consume\Import\Process\ConfigurableProcessorInterface to have those
 * configurations applied. This allows processor instances to be created by
 * dependency injection and the ClassResover.
 *
 * @see \Drupal\consume\Import\Process\ConfigurableProcessorInterface
 */
interface ProcessorInterface {

  /**
   * Attach the processor to importer and operation it is a part of.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer this processor and its operation are contained in.
   * @param \Drupal\consume\Import\OperationInterface $operation
   *   The operation this processor is a part of.
   *
   * @return self
   *   Return this instance for method chaining.
   */
  public function setup(ImporterInterface $importer, OperationInterface $operation): self;

  /**
   * Process data before the properties are mapped.
   *
   * @param array $data
   *   Reference to the data to process before the data is mapped.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   The data mappers used by this import operation.
   *
   * @throws \Drupal\consume\Import\Exception\IgnoreRowException
   * @throws \Drupal\consume\Import\Exception\InvalidDataRowException
   * @throws \Drupal\consume\Import\Exception\ResumeableException
   * @throws \Drupal\consume\Import\Exception\SkipRowException
   */
  public function preprocess(array &$data, array $mappers): void;

  /**
   * Process data after the properties have been mapped, but before written.
   *
   * @param array $data
   *   Reference to the data to process before the data is mapped.
   * @param array $mappedData
   *   Reference to the mapped data, before it is written.
   *
   * @throws \Drupal\consume\Import\Exception\IgnoreRowException
   * @throws \Drupal\consume\Import\Exception\InvalidDataRowException
   * @throws \Drupal\consume\Import\Exception\ResumeableException
   * @throws \Drupal\consume\Import\Exception\SkipRowException
   */
  public function process(array $data, array &$mappedData): void;

  /**
   * Process information about the tracking data.
   *
   * @param array $data
   *   The original data from the datasource.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   The data mappers to transfer fields with.
   * @param \Drupal\consume\Import\Tracker\TrackerInterface $tracker
   *   The import instance to track site changes.
   * @param array $tracking
   *   The tracking data fetched by the tracker.
   *
   * @throws \Drupal\consume\Import\Exception\IgnoreRowException
   * @throws \Drupal\consume\Import\Exception\ResumeableException
   * @throws \Drupal\consume\Import\Exception\SkipRowException
   */
  public function processTracking(array $data, array $mappers, TrackerInterface $tracker, array &$tracking): void;

}
