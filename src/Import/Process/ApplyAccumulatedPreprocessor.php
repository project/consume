<?php

namespace Drupal\consume\Import\Process;

use Drupal\consume\Import\ConfigurableInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Add data from an storage accumulator before data is mapped.
 *
 * In order to support data from multiple datasources (e.g. multiple files),
 * we can store the data in storage accumulators (an example would be the
 * Writer plugin "data_accumulator"). This processor applies data accumulated
 * into a database table.
 *
 * The processor adds the accumulated data to the mapped data in the
 * ::preprocess() method.
 *
 * @see \Drupal\consume\Plugin\Consume\Writer\DataAccumulator
 */
class ApplyAccumulatedPreprocessor extends AccumulatedProcessorBase implements ProcessorInterface, ConfigurableInterface, ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$data, array $mappers): void {
    $idField = $this->configuration['id'];

    if (!empty($data[$idField])) {
      $data += $this->fetchValues($data[$idField]);
    }
  }

}
