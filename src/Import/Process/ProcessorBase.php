<?php

namespace Drupal\consume\Import\Process;

use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\OperationInterface;
use Drupal\consume\Import\Tracker\TrackerInterface;

/**
 * Base processor implementation.
 *
 * Implements all the processor methods so subclasses can only need to define
 * the processor methods they need instead of explicitly defining them all.
 */
abstract class ProcessorBase implements ProcessorInterface {

  /**
   * The ID of the importer instance this import process is part of.
   *
   * @var string|null
   */
  public ?string $importId;

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer, OperationInterface $operation): self {
    $this->importId = $importer->id();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$data, array $mappers): void {
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $data, array &$mappedData): void {
  }

  /**
   * {@inheritdoc}
   */
  public function processTracking(array $data, array $mappedData, TrackerInterface $tracker, array &$tracking): void {
  }

}
