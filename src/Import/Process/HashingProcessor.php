<?php

namespace Drupal\consume\Import\Process;

use Drupal\consume\Import\Exception\SkipRowException;
use Drupal\consume\Import\Tracker\HashingTrackerInterface;
use Drupal\consume\Import\Tracker\TrackerInterface;

/**
 * A data processor that computes and compares the data hashes.
 */
class HashingProcessor extends ProcessorBase implements ProcessorInterface {

  /**
   * Compute a hash from the data record.
   *
   * @param array $data
   *   The data to compute the hash from.
   *
   * @return string
   *   A computed checksum to compare and store to detect data changes.
   */
  public function computeHash(array $data): string {
    static::canonicalizeData($data);

    // Ensure that we are explicit with the encoding flags so this remains
    // consistent in the future regardless of the defaults or Drupal
    // specifications changing.
    $encodeFlags = JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    return md5(json_encode($data, $encodeFlags));
  }

  /**
   * Recursively sort array by key.
   *
   * @param array $data
   *   The data to canonicalize.
   */
  protected static function canonicalizeData(array &$data): void {
    foreach ($data as &$value) {
      if (is_array($value)) {
        self::canonicalizeData($value);
      }
    }
    ksort($data);
  }

  /**
   * {@inheritdoc}
   */
  public function processTracking(array $data, array $mappers, TrackerInterface $tracker, array &$tracking): void {
    if ($tracker instanceof HashingTrackerInterface && $tracker->allowHash()) {
      $hashable = [];
      foreach ($mappers as $mapper) {
        $mapper->transfer($data, $hashable);
      }
      $dataHash = $this->computeHash($hashable);

      // Compare the hash to existing data, and skip if they match.
      if (isset($tracking['hash']) && $tracking['hash'] == $dataHash) {
        throw new SkipRowException(SkipRowException::CHECKSUM, TRUE);
      }

      // Update the tracking hash for use with future change detection.
      $tracking['hash'] = $dataHash;
    }
  }

}
