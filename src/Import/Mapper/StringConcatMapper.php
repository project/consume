<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Concat string values together with a "glue" value (calls "implode()").
 */
class StringConcatMapper extends MapValuesMapper {

  /**
   * The string value to use as the glue when concatenating string parts.
   *
   * @var string
   */
  protected string $glue;

  /**
   * Options on how to join the string.
   *
   * Currently the only option is "trim" to trim the string parts before
   * joining the parts together.
   *
   * @var array
   */
  protected array $options;

  /**
   * Create a new instance of the StringConcatMapper class.
   *
   * @param string $dst_key
   *   The key to the destination file to place the value into.
   * @param array $values_map
   *   The mapping of values from the source to pull. Order will determine the
   *   order of the concatenated string parts.
   * @param string $glue
   *   The string "glue" to use when joining the string parts.
   * @param array $options
   *   The mapper options.
   */
  public function __construct(string $dst_key, array $values_map, string $glue = ' ', array $options = []) {
    parent::__construct($dst_key, $values_map);

    $this->glue = $glue;
    $this->options = $options + [
      'trim' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($values): mixed {
    if (!empty($this->options['trim'])) {
      $values = array_map('trim', array_filter($values));
    }

    return implode($this->glue, $values);
  }

}
