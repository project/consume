<?php

namespace Drupal\consume\Import\Mapper;

use Drupal\Component\Utility\NestedArray;

/**
 * Trait to handle setting of destination values into nest arrays.
 *
 * This allows data mappers to handle nested array destination values in a
 * consistent way, and abstract handling of the different destination key
 * formats.
 */
abstract class MapperBase implements DataMapperInterface {

  /**
   * The index key base name for the destination array.
   *
   * This is the top level index, or field that the value should be placed.
   *
   * @var string
   */
  protected string $dstKeyBase;

  /**
   * If the parent indexes if the destination key points to a nested array.
   *
   * If the destination key is "field[0][1][value]" this will be an array with
   * values [0, 1, "value"]. If the last index is empty (array append, e.g.
   * "field[0][]") then the empty value index is excluded, and the value will
   * be appended to the destination.
   *
   * @var array
   *
   * @see self::setDstKey()
   */
  protected array $dstIndexes = [];

  /**
   * The destination setting method.
   *
   * This value is set during self::setDstKey() based on the original
   * destination key value.
   *
   * @var string
   */
  protected string $setDstMethod = 'setDestinationValue';

  /**
   * {@inheritdoc}
   */
  public function getKey(): string {
    $key = $this->dstKeyBase;

    if ($this->dstIndexes) {
      $key .= '[' . implode('][', $this->dstIndexes) . ']';
    }
    if ($this->setDstMethod === 'addDestinationIndexValue') {
      $key .= '[]';
    }

    return $key;
  }

  /**
   * Get the values form the source that is used by this mapper.
   *
   * @param array $data
   *   The source data to get extracted data from.
   *
   * @return mixed
   *   The extracted values.
   */
  abstract public function extractValues(array $data): mixed;

  /**
   * {@inheritdoc}
   */
  public function transfer(array $data, array &$dst): void {
    $this->setDestination($this->extractValues($data), $dst);
  }

  /**
   * {@inheritdoc}
   */
  public function map(array $data, array &$dst, array $mappers = []): void {
    $transformed = $this->transform($this->extractValues($data));
    $this->setDestination($transformed, $dst);
  }

  /**
   * Method to call set the destination value.
   *
   * Normally this is the method to be called directly from a mapper, and the
   * other protected setter methods are meant to be configured by the
   * self::setDstKey() based on the raw key string.
   *
   * @param mixed $value
   *   The value to set.
   * @param array $dst
   *   Reference to the destination array to place the value.
   */
  public function setDestination($value, array &$dst): void {
    $method = $this->setDstMethod;
    $this->{$method}($value, $dst);
  }

  /**
   * Set the mapper destination key information.
   *
   * This method decodes the destination string, allowing the use of nested
   * array indexes and setting the appropriate callback setter method based
   * on the index type.
   *
   * @param string $key
   *   The raw string representation of where to place the mapped value.
   */
  protected function setDestinationKey(string $key): void {
    $parts = explode('[', $key, 2);
    if (isset($parts[1])) {
      if (']' === $parts[1]) {
        $this->dstIndexes = [];
        $this->setDstMethod = 'addDestinationIndexValue';
      }
      else {
        $parts[1] = rtrim($parts[1], '] ');
        $indexes = preg_split('/\]\[/', $parts[1]);
        $this->dstIndexes = $indexes;

        $last = end($this->dstIndexes);
        if (NULL === $last || '' === $last) {
          array_pop($this->dstIndexes);
          $this->setDstMethod = 'addDestinationIndexValue';
        }
        else {
          $this->setDstMethod = 'setDestinationIndexValue';
        }

        // Only the last key index can be empty. If there are any empty index
        // keys notify the import builder that there is a problem building
        // this importer.
        if ($this->dstIndexes != array_filter($this->dstIndexes, 'strlen')) {
          $errMsg = sprintf('The destination key "%s" has empty indexes. Only the last array key can be left empty to append values.', $key);
          throw new \InvalidArgumentException($errMsg);
        }
      }
    }
    else {
      // No indexes, just set the value directly.
      $this->setDstMethod = 'setDestinationValue';
    }

    $this->dstKeyBase = $parts[0];
  }

  /**
   * Callback to set a nested array value.
   *
   * @param mixed $value
   *   The value to set.
   * @param array $dst
   *   The destination array to place the value.
   */
  protected function setDestinationIndexValue($value, array &$dst): void {
    if (empty($dst[$this->dstKeyBase])) {
      $dst[$this->dstKeyBase] = [];
    }

    NestedArray::setValue($dst[$this->dstKeyBase], $this->dstIndexes, $value);
  }

  /**
   * Callback to append a value to a nested array.
   *
   * This is used when the last index key is empty. Only the last array key
   * is allowed to be empty.
   *
   * @param mixed $value
   *   The value to set.
   * @param array $dst
   *   The destination array to place the value.
   */
  protected function addDestinationIndexValue($value, array &$dst): void {
    if (empty($dst[$this->dstKeyBase])) {
      $dst[$this->dstKeyBase] = [];
    }

    if (!isset($value)) {
      return;
    }

    $ref = &$dst[$this->dstKeyBase];
    foreach ($this->dstIndexes as $key) {
      if (!is_array($ref) || !(isset($ref[$key]) && array_key_exists($key, $ref))) {
        $ref[$key] = [];
      }
      $ref = &$ref[$key];
    }

    $ref[] = $value;
  }

  /**
   * Callback to set the full value of the referenced destination location.
   *
   * @param mixed $value
   *   The value to set.
   * @param array $dst
   *   The destination array to place the value.
   */
  protected function setDestinationValue($value, array &$dst): void {
    $dst[$this->dstKeyBase] = $value;
  }

}
