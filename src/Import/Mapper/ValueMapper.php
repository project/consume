<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Simple data mapper, to move a value into a destination array.
 */
class ValueMapper extends MapperBase implements DataMapperInterface {

  /**
   * Array index to use when placing the value into destination array.
   *
   * @var string
   */
  protected $dstKey;

  /**
   * The name of the source value to fetch the raw data from.
   *
   * @var string
   */
  protected $srcKey;

  /**
   * Generate a new value ValueMapper instance.
   *
   * @param string $dst_key
   *   The string name of the destination key.
   * @param string $src_key
   *   The string name of the source value key.
   */
  public function __construct($dst_key, $src_key) {
    $this->srcKey = $src_key;
    $this->setDestinationKey($dst_key);
  }

  /**
   * {@inheritdoc}
   */
  public function bindRefs(array $mappers = []): void {
    // @todo Bind data references.
  }

  /**
   * {@inheritdoc}
   */
  public function transform($values): mixed {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function extractValues(array $data): mixed {
    return $data[$this->srcKey] ?? '';
  }

}
