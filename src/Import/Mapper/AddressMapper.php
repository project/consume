<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Maps array data into a format that is accepted by the address field.
 */
class AddressMapper extends MapperBase implements DataMapperInterface {

  /**
   * Array that maps source data keys to their address component counterparts.
   *
   * @var array
   */
  protected array $addressMap;

  /**
   * Default address values.
   *
   * @var array
   */
  protected array $defaults;

  /**
   * Create a new instance of an AddressMapper.
   *
   * @param string $dst_key
   *   The name of the destination key.
   * @param array $address_map
   *   A map of where to find the address components from the source array.
   * @param array $defaults
   *   Default address values.
   */
  public function __construct($dst_key, array $address_map, array $defaults = ['country_code' => 'US']) {
    $this->addressMap = $address_map;
    $this->defaults = $defaults;

    $this->setDestinationKey($dst_key);
  }

  /**
   * {@inheritdoc}
   */
  public function bindRefs(array $mappers = []): void {
    // @todo Bind data references.
  }

  /**
   * {@inheritdoc}
   */
  public function transform($values): mixed {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function extractValues(array $data): mixed {
    $address = $this->defaults;

    foreach ($this->addressMap as $to => $from) {
      if ($to === 'postal_code' && is_array($from)) {
        if (!empty($data[$from[0]])) {
          $address[$to] = str_pad($data[$from[0]], 5, '0', STR_PAD_LEFT);

          if (!empty($data[$from[1]])) {
            $address[$to] .= '-' . str_pad($data[$from[1]], 4, '0', STR_PAD_LEFT);
          }
        }
      }
      elseif (!is_array($from) && !empty($data[$from])) {
        $address[$to] = $data[$from];
      }
    }

    return $address;
  }

}
