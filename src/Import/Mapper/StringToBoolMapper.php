<?php

namespace Drupal\consume\Import\Mapper;

/**
 * A data mapper that moves multiple source items into one destination key.
 */
class StringToBoolMapper extends ValueMapper {

  /**
   * Strings that equate to TRUE.
   *
   * @var string[]
   */
  protected $trueStr;

  /**
   * Strings that equate to FALSE.
   *
   * @var string[]
   */
  protected $falseStr;

  /**
   * Value to use if string does not match either sets of string values.
   *
   * @var bool
   */
  protected $defaultVal;

  /**
   * Maps data from the destination array back into the destination data.
   *
   * @param string $dst_key
   *   The string name of the destination key.
   * @param string $src_key
   *   The string name of the destination key.
   * @param string[] $true_values
   *   Array of strings that equate to TRUE.
   * @param string[] $false_values
   *   Array of strings that equate to FALSE.
   * @param bool $default
   *   If value doesn't match either set of strings, use this value.
   */
  public function __construct($dst_key, $src_key, array $true_values, array $false_values = [], $default = FALSE) {
    parent::__construct($dst_key, $src_key);

    $this->trueStr = $true_values;
    $this->falseStr = $false_values;
    $this->defaultVal = $default;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if (!empty($value)) {
      $value = strtolower($value);

      if (!$this->defaultVal && in_array($value, $this->trueStr)) {
        return TRUE;
      }
      elseif ($this->defaultVal && in_array($value, $this->falseStr)) {
        return FALSE;
      }
    }

    return $this->defaultVal;
  }

}
