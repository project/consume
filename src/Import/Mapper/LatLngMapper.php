<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Transform latitude and longitude source values into a WRT point format.
 *
 * Transform latitude and longitude source values to the WRT point,
 * which is the accepted value format geofields and other geo spatial field
 * types.
 *
 * The expected source keys for this field is:
 *   [
 *     'lng' => <source longitude key>,
 *     'lat' => <source latitude key>,
 *   ]
 * This way the values mapper will know where to find the coordinate values
 * from the data source.
 */
class LatLngMapper extends MapValuesMapper {

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    return (isset($value['lng']) && isset($value['lat']))
      ? "POINT({$value['lng']} {$value['lat']})"
      : NULL;
  }

}
