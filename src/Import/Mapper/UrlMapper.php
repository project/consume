<?php

namespace Drupal\consume\Import\Mapper;

use Drupal\Component\Utility\UrlHelper;

/**
 * Mapper for transferring URLs and ensuring they are in a valid format.
 *
 * Invalid URLs will get transformed to NULL, and won't get imported.
 *
 * @todo Need to report invalid URLs so administrators can update and fix this
 * these data issues if found.
 */
class UrlMapper extends ValueMapper {

  /**
   * URL parsing options.
   *
   * Sets the allowed protocols and if "http" should be converted to "https".
   *
   * @var array
   */
  protected array $options;

  /**
   * Generate a new value UrlMapper instance.
   *
   * @param string $dst_key
   *   The string name of the destination key.
   * @param string $src_key
   *   The string name of the source value key.
   * @param array $options
   *   The URL parsing options.
   */
  public function __construct($dst_key, $src_key, array $options = []) {
    parent::__construct($dst_key, $src_key);

    $this->options = $options + [
      'allowed_protocols' => ['http', 'https'],
      'default_protocol' => 'https',
      'force_https' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if (!empty($value) && is_string($value)) {
      $url = trim($value);

      if (preg_match('#^(?:([a-z]+):)?//(.*)$#i', $url, $matches)) {
        if (!in_array($matches[1], $this->options['allowed_protocols'])) {
          return NULL;
        }

        if ($this->options['force_https'] && (empty($matches[1]) || 'http' === $matches[1])) {
          $url = 'https://' . $matches[2];
        }
      }
      elseif ($url[0] != '/') {
        $url = $this->options['default_protocol'] . '://' . $url;
      }

      if (UrlHelper::isValid($url)) {
        return $url;
      }
    }

    return NULL;
  }

}
