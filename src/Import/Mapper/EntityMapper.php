<?php

namespace Drupal\consume\Import\Mapper;

use Drupal\consume\Import\Exception\ResumeableException;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Map values to a content entity instance.
 */
class EntityMapper extends MapValuesMapper {

  /**
   * If $options['use_lookup'] is enabled, a lookup table of known entities.
   *
   * @var array
   */
  protected $lookup = [];

  /**
   * Entity matching and search options.
   *
   * Mapping options for content entity matching and usage.
   *  allow_create: Allow entities to be created if they don't exist.
   *  published: Should new entities be published or unpublished.
   *  use_lookup: Utilize and maintain an entity look-up keyed by the ID field.
   *  persist_lookup: Should the lookup table be serialized and kept?
   *
   * Defaults are applied and can be seen in the constructor.
   *
   * @var mixed[]
   */
  protected array $options;

  /**
   * The machine name of the entity type.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * The machine name of the entity bundle to load.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity fields to match when searching for existing entities.
   *
   * @var string
   */
  protected $field;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * Maps values to an entity type and bundle.
   *
   * @param string $dst_key
   *   The string name of the destination key.
   * @param array $values_map
   *   The string name of the destination key.
   * @param string $entity_type
   *   The entity type of the entities to locate or create.
   * @param string $bundle
   *   The entity bundle to create or find entity instances for.
   * @param string $id_field
   *   The data property to use for locating entity instances with.
   * @param array $options
   *   Entity mapper options. Options determine if the look-up cache is used,
   *   if terms are allowed to be created, and if they are published when
   *   created.
   */
  public function __construct(string $dst_key, array $values_map, string $entity_type, string $bundle, $id_field = 'name', array $options = []) {
    parent::__construct($dst_key, $values_map, $options['multiple'] ?? FALSE);

    $this->entityType = $entity_type;
    $this->bundle = $bundle;
    $this->field = $id_field;

    // Apply option defaults.
    $this->options = $options + [
      'allow_create' => FALSE,
      'published' => TRUE,
      'use_lookup' => TRUE,
      'persist_lookup' => FALSE,
    ];
  }

  /**
   * Implements the __sleep() magic PHP method.
   *
   * @return string[]
   *   The object properties to serialize.
   */
  public function __sleep() {
    $vars = get_object_vars($this);

    // Keeping the lookup tables across batch operation can help keep things
    // faster, however, this lookup can potentially get big and might need to
    // get flushed.
    if (!empty($this->options['persist_lookup'])) {
      unset($vars['lookup']);
    }

    // Do not persist the entity type manager service.
    unset($vars['entityTypeManager']);

    return array_keys($vars);
  }

  /**
   * Gets the entity type manager service instance.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Gets the entity type manager service.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Gets the entity definition for the target content entity type.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface
   *   The content entity definition for the target entity type.
   */
  protected function getEntityDefinition(): ContentEntityTypeInterface {
    return $this
      ->getEntityTypeManager()
      ->getDefinition($this->entityType);
  }

  /**
   * Get the entity storage handler for the target entity type.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage handler for the target entity type.
   */
  protected function getEntityStorage(): EntityStorageInterface {
    return $this
      ->getEntityTypeManager()
      ->getStorage($this->entityType);
  }

  /**
   * {@inheritdoc}
   */
  public function transform($values): mixed {
    $refs = [];
    if ($this->isMulti) {
      foreach ($values as $idx => $item) {
        if ($entity = $this->getEntity($item)) {
          $refs[$idx] = $entity;
        }
      }
    }
    else {
      $refs[] = $this->getEntity($values);
    }

    return $refs;
  }

  /**
   * Get the matching entity to the values.
   *
   * The method can either find a matching entity or (if allowed) create a new
   * instance of the entity.
   *
   * @param array $values
   *   The values to use match up the entity or create a new instance with.
   *
   * @return array|null
   *   Create an entity field reference item value (just the "target_id" value).
   *   Will return NULL if a matching entity is not available or could not be
   *   created.
   */
  protected function getEntity(array $values): ?array {
    if (empty($values[$this->field])) {
      return NULL;
    }

    try {
      $value = $values[$this->field];
      $find = is_string($value) ? strtolower($value) : $value;

      // Was entity previously discovered? Use the lookup results.
      if ($this->options['use_lookup'] && !empty($this->lookup[$find])) {
        return ['target_id' => $this->lookup[$find]];
      }

      $entityType = $this->getEntityDefinition();
      $storage = $this->getEntityStorage();

      $properties = [$this->field => $value];
      if ($entityType->hasKey('bundle')) {
        $properties[$entityType->getKey('bundle')] = $this->bundle;
      }

      // Try to find the term by the key field.
      /** @var \Drupal\Core\Entity\EntityInterface[] $entities */
      $entities = $storage->loadByProperties($properties);
      $entity = reset($entities);

      if (empty($entity) && $this->options['allow_create']) {
        $labelKey = $entityType->getKey('label');
        // Ensure that new entity has a value for the label field.
        if ($labelKey && $this->field !== $labelKey) {
          $properties[$labelKey] = $values[$labelKey] ?? $values['label'] ?? $value;
        }
        $properties += $values;

        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $storage->create($properties);

        // Set the publishing status of the new term.
        if ($this->options['published']) {
          if ($entity instanceof EntityPublishedInterface) {
            $entity->setPublished();
          }
          elseif ($entityType->hasKey('status')) {
            $entity->set($entityType->getKey('status'), TRUE);
          }
        }
        $entity->save();
      }

      if ($entity) {
        // If the term lookup is being used, update this key value time.
        if ($this->options['use_lookup']) {
          $this->lookup[$find] = $entity->id();
        }

        return ['target_id' => $entity->id()];
      }

      return NULL;
    }
    catch (\Exception $e) {
      // Allow import to continue working, but still report that an error
      // occurred.
      //
      // Rethrowing as a ResumeableException allows to caller to log the error
      // while knowing that this mapper believe that it should be okay to
      // continue trying the rest of the import with an empty term.
      //
      // An empty required term field will be caught during entity validation
      // later when the import builds the full entity.
      throw new ResumeableException('Unable to query or create entity.', 0, $e);
    }
  }

}
