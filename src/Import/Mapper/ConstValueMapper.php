<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Simple data mapper, to move place a constant value into the destination.
 */
class ConstValueMapper extends ValueMapper {

  /**
   * The name of the source value to fetch the raw data from.
   *
   * @var mixed
   */
  protected $value;

  /**
   * Generate a new ConstValueMapper.
   *
   * @param string $dst_key
   *   The string name of the destination key.
   * @param mixed $value
   *   The value to place into the destination.
   */
  public function __construct($dst_key, $value) {
    $this->value = $value;
    $this->setDestinationKey($dst_key);
  }

  /**
   * {@inheritdoc}
   */
  public function extractValues(array $data): mixed {
    // Always return the static value set in the constructor.
    return $this->value;
  }

}
