<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Maps and transforms data from the source array to a destination.
 *
 * Data mappers are meant to translate and combine data columns for a single
 * destination field or property. If you are looking to apply business logic
 * with the data, or determine if a record should be skipped, use a "processor"
 * instead.
 *
 * @see \Drupal\consume\Import\Process\ProcessorInterface
 */
interface DataMapperInterface {

  /**
   * Retrieves the string name of the destination key for this mapper.
   *
   * @return string
   *   The string name of the field to place the mapped value into.
   */
  public function getKey();

  /**
   * Allow this mapper reference other data mappers.
   *
   * This allows the potential for mappers to perform chaining of mappers and
   * building complex data transforms.
   *
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   Available mapper instances to bind and reference.
   */
  public function bindRefs(array $mappers = []): void;

  /**
   * Transforms values.
   *
   * @param mixed $values
   *   Values to transform into the value.
   *
   * @return mixed
   *   The transformed value.
   */
  public function transform($values): mixed;

  /**
   * Just move the data managed by this mapper to the destination.
   *
   * @param array $data
   *   The raw data to transfer.
   * @param array $dst
   *   Reference to the destination array that the data is transferred to.
   */
  public function transfer(array $data, array &$dst): void;

  /**
   * Maps data to a destination by transforming and translating it.
   *
   * @param array $data
   *   The raw data to re-organize.
   * @param array $dst
   *   Reference to the destination array. Transformed data will be placed in.
   * @param \Drupal\consume\Import\Mapper\DataMapperInterface[] $mappers
   *   Other data mappers used with this import batch for advanced chaining or
   *   reference resolutions.
   */
  public function map(array $data, array &$dst, array $mappers = []): void;

}
