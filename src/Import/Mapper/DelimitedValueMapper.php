<?php

namespace Drupal\consume\Import\Mapper;

/**
 * A data mapper for splitting a delimited string into an array.
 */
class DelimitedValueMapper extends ValueMapper {

  /**
   * String delimiter being used to separate the values.
   *
   * The string will be separated along this delimiter, and transformed into
   * an array value.
   *
   * @var string
   */
  protected $delimiter;

  /**
   * Should the separated values be trimmed with "trim()".
   *
   * @var bool
   */
  protected $useTrim;

  /**
   * Maps data from the destination array back into the destination data.
   *
   * @param string $dst_key
   *   The string name of the destination array key.
   * @param string $src_key
   *   The array key of the source data to get the value from.
   * @param string $delimiter
   *   The delimiter to separate the value into separate items.
   * @param bool $use_trim
   *   Apply string trimming to the exploded values?
   */
  public function __construct($dst_key, $src_key, $delimiter = ',', bool $use_trim = TRUE) {
    parent::__construct($dst_key, $src_key);

    $this->delimiter = $delimiter;
    $this->useTrim = $use_trim;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if (!empty($value)) {
      $result = explode($this->delimiter, $value);

      if ($this->useTrim) {
        return array_map('trim', $result);
      }

      return $result;
    }

    return NULL;
  }

}
