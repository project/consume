<?php

namespace Drupal\consume\Import\Mapper;

/**
 * A data mapper replaces values which match a regular expression.
 *
 * This allows transforming known set of string, or a pattern of values into
 * a consistent value for writing.
 */
class StringReplaceMapper extends ValueMapper {

  /**
   * The regular expression to use when looking for string matches.
   *
   * Can be an array of multiple pattern should be checked, see preg_replace()
   * method for more information about how multiple patterns are used.
   *
   * @var string|array
   *
   * @see preg_replace()
   */
  protected $pattern;

  /**
   * The string replacement to replace matches with. Empty if removing matches.
   *
   * Can be an array if being used with multiple patterns. Replacements will be
   * matched with an array of patterns. See preg_replace() for info about how
   * patterns and replacements are used.
   *
   * @var string
   */
  protected $replace;

  /**
   * Maps data from the destination array back into the destination data.
   *
   * @param string $dst_key
   *   The string name of the destination array key.
   * @param string $src_key
   *   The array key of the source data to get the value from.
   * @param string|array $pattern
   *   The regex expressions use to match and find strings to replace.
   * @param string|array $replace
   *   The replacement values to switch with the pattern matches. If empty,
   *   matches are removed from the string.
   */
  public function __construct($dst_key, $src_key, $pattern, $replace = NULL) {
    parent::__construct($dst_key, $src_key);

    if (is_array($pattern) && !isset($replace)) {
      $this->pattern = array_keys($pattern);
      $this->replace = array_values($pattern);
    }
    else {
      $this->pattern = $pattern;
      $this->replace = $replace;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if (!empty($value)) {
      return preg_replace($this->pattern, $this->replace, $value);
    }

    return NULL;
  }

}
