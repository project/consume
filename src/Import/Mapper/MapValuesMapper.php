<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Transfer multiple source values into a single array of values.
 *
 * Transer values from the source data into an array of values, and assign
 * the mapped values array into the destination key.
 */
class MapValuesMapper extends MapperBase implements DataMapperInterface {

  /**
   * Array values should be added to produce an array of value sets.
   *
   * For instance a value map of:
   *   $this->valuesMap = [
   *     'key1' => 'source_1',
   *     'key2' => 'source_2',
   *   ];
   *
   * will result in mapping values to:
   *   $dst[$this->dstKey] = [
   *     [
   *        'key1' => $data['source_1'][0],
   *        'key2' => $data['source_2'][0],
   *     ],
   *     [
   *        'key1' => $data['source_1'][1],
   *        'key2' => $data['source_2'][1],
   *     ],
   *   ];
   *
   * Which allows for the ability to combine and build multiple value arrays.
   *
   * @var bool
   */
  protected bool $isMulti;

  /**
   * Map values keys to data source keys.
   *
   * The array keys are the keys to place the source data values into. The array
   * value, are the data source keys to get the data values from.
   *
   * For instance a value map of:
   *   $this->valuesMap = [
   *     'key1' => 'source_1',
   *     'key2' => 'source_2',
   *   ]
   *
   * will result in mapping values to:
   *   $dst[$this->dstKey] = [
   *     'key1' => $data['source_1'],
   *     'key2' => $data['source_2'],
   *   ]
   *
   * The resulting value array is assigned to the mapped destination.
   *
   * @var array
   */
  protected $valuesMap;

  /**
   * Create a new instance of a MapValuesMapper.
   *
   * @param string $dst_key
   *   The name of the destination key to place values array into.
   * @param string[] $values_map
   *   A map of data to map into the values array. The array keys are the
   *   destination keys, and the values are the keys from the source data.
   * @param bool $isMulti
   *   Should items in the $values_map be handled as a multi-valued array?
   */
  public function __construct($dst_key, array $values_map, bool $isMulti = FALSE) {
    $this->valuesMap = $values_map;
    $this->isMulti = $isMulti;
    $this->setDestinationKey($dst_key);
  }

  /**
   * {@inheritdoc}
   */
  public function bindRefs(array $mappers = []): void {
    // @todo Bind data references.
  }

  /**
   * {@inheritdoc}
   */
  public function transform($values): mixed {
    return $values;
  }

  /**
   * Apply the values mapping to create a values array.
   *
   * @param array $data
   *   The source data to extract the values from.
   *
   * @return array
   *   An array of values mapped from data using this mapper's $valuesMap.
   */
  public function extractValues(array $data): mixed {
    $values = [];

    if ($this->isMulti) {
      $monoValues = [];
      foreach ($this->valuesMap as $dst => $src) {
        if (isset($data[$src])) {
          $srcVal = $data[$src];

          if (is_array($data)) {
            foreach ($srcVal as $idx => $value) {
              $values[$idx][$dst] = $value;
            }
          }
          else {
            $monoValues[$dst] = $srcVal;
          }
        }
      }

      // If we have any single value, apply them across destination values.
      if ($monoValues) {
        if (!$values) {
          $values[] = [];
        }

        foreach ($monoValues as $dst => $value) {
          foreach ($values as &$item) {
            $item[$dst] = $value;
          }
        }
      }
    }
    else {
      foreach ($this->valuesMap as $dst => $src) {
        if (!empty($data[$src])) {
          $values[$dst] = $data[$src];
        }
      }
    }

    return $values;
  }

}
