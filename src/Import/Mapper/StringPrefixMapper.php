<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Add the prefix to values.
 */
class StringPrefixMapper extends ValueMapper {

  /**
   * The prefix to add to all the values.
   *
   * @var string
   */
  protected string $prefix;

  /**
   * Create a new instance of the StringConcatMapper class.
   *
   * @param string $dst_key
   *   The key to the destination file to place the value into.
   * @param string $src_key
   *   Key to the source data to fetch the values from.
   * @param string $prefix
   *   The prefix to all the string values.
   */
  public function __construct(string $dst_key, string $src_key, string $prefix) {
    parent::__construct($dst_key, $src_key);

    $this->prefix = $prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if (!empty($value)) {
      return $this->prefix . $value;
    }

    return NULL;
  }

}
