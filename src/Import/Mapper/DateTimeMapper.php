<?php

namespace Drupal\consume\Import\Mapper;

/**
 * Transform time values into a standard datetime format.
 */
class DateTimeMapper extends ValueMapper {

  /**
   * String representing a date time format to translate to.
   *
   * @var string
   */
  protected $format = 'Y-m-d\TH:i:s';

  /**
   * The configured timezone to use while translating the date time data.
   *
   * @var \DateTimeZone
   */
  protected $tz = NULL;

  /**
   * Create a new data mapper for ensure a proper datetime format for values.
   *
   * @param string $dst_key
   *   Key for the destination to map the data to.
   * @param string $src_key
   *   An array of key names for source values and their respective destination.
   * @param string $format
   *   Datetime format to use when creating the date.
   * @param string|null $tz
   *   Timezone string of the source dates being imported, UTC assumed if NULL.
   */
  public function __construct($dst_key, $src_key, string $format = 'Y-m-d\TH:i:s', ?string $tz = NULL) {
    parent::__construct($dst_key, $src_key);

    $this->format = $format;
    if ($tz) {
      $this->tz = new \DateTimeZone($tz);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value): mixed {
    if ($value) {
      if ($this->tz) {
        $offset = new \DateTimeZone('UTC');

        // Ensure datetime formatting for all included time fields.
        $date = new \DateTime($value, $this->tz);
        $date->setTimezone($offset);
        return $date->format($this->format);
      }

      return date($this->format, strtotime($value));
    }

    return NULL;
  }

}
