<?php

namespace Drupal\consume\Import;

/**
 * Import handlers with configurations.
 *
 * Use the "get" and "set" methods when handlers need to have configurations
 * injected. This allows the processor to implement
 * \Drupal\Core\DependencyInjection\ContainerInjectionInterface and utilize
 * the ClassResolver to generate instances.
 */
interface ConfigurableInterface {

  /**
   * Gets the processor configuration.
   *
   * @return array
   *   Get the current import handler configurations.
   */
  public function getConfiguration(): array;

  /**
   * Add processor configurations and options for processors that have them.
   *
   * @param array $config
   *   Processor configurations, and settings.
   *
   * @return self
   *   The configurable handler itself to support method chaining.
   */
  public function setConfiguration(array $config): self;

}
