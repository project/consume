<?php

namespace Drupal\consume\Import;

use Drupal\consume\Import\Exception\BatchAlreadyRunningException;

/**
 * Trait implementing the acquire and release method for an exclusive importer.
 *
 * @see \Drupal\consume\Import\ExclusiveImporterInterface
 */
trait ExclusiveImporterTrait {

  /**
   * Time in seconds to hold the lock before timing out.
   *
   * @var int
   */
  protected $lockTimeout = 3600;

  /**
   * The persistent backend lock manager.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lockService;

  /**
   * Get an instance of the lock backend to use for locking acquisition.
   *
   * @return \Drupal\Core\Lock\LockBackendInterface
   *   The persistent lock backend.
   */
  protected function getLockService() {
    if (!isset($this->lockService)) {
      $this->lockService = \Drupal::service('lock.persistent');
    }

    return $this->lockService;
  }

  /**
   * Get the name of the import, used for identifying the lock name.
   *
   * @return string
   *   The batch identifier to lock on.
   */
  abstract public function getLockId();

  /**
   * Check the status of the importer lock.
   *
   * @return bool
   *   Is the lock available for acquiring.
   */
  public function isLockAvailable(): bool {
    $lock = $this->getLockService();
    return $lock->lockMayBeAvailable($this->getLockId());
  }

  /**
   * Aquire the exclusive import lock if possible.
   *
   * @see \Drupal\consume\Import\ExclusiveImporterInterface::acquireLock()
   * @see \Drupal\consume\Import\ImporterBatchBase::init()
   */
  public function acquireLock(): void {
    $lock = $this->getLockService();

    if (!$lock->acquire($this->getLockId(), $this->lockTimeout)) {
      throw new BatchAlreadyRunningException();
    }
  }

  /**
   * Release the import lock if one was captured.
   *
   * @see \Drupal\consume\Import\ExclusiveImporterInterface::releaseLock()
   * @see \Drupal\consume\Import\ImporterBatchBase::finalize()
   */
  public function releaseLock(): void {
    $lock = $this->getLockService();
    $lock->release($this->getLockId());
  }

}
