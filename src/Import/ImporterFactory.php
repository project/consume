<?php

namespace Drupal\consume\Import;

use Drupal\toolshed\Strategy\ContainerStrategyFactory;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyFactoryInterface;
use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Strategy factory for generating consume importer instances.
 */
class ImporterFactory extends ContainerStrategyFactory implements StrategyFactoryInterface {

  /**
   * The import operation factory.
   *
   * @var \Drupal\consume\Import\ImportOperationFactory
   */
  protected ImportOperationFactory $opFactory;

  /**
   * Creates a new ImporterFactory instance.
   *
   * @param \Drupal\consume\Import\ImportOperationFactory $operation_factory
   *   The import operation factory to build operations with.
   */
  public function __construct(ImportOperationFactory $operation_factory) {
    parent::__construct();

    $this->opFactory = $operation_factory;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\consume\Import\ImporterInterface
   *   The importer instance built from the importer definition.
   */
  public function create(string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    /** @var \Drupal\consume\Import\ImporterInterface $importer */
    $importer = parent::create($id, $definition);

    // Construct and add import operations.
    foreach ($definition->operations ?? [] as $opId => $opDef) {
      $operation = $this->opFactory->createOperation($opId, $opDef);
      $importer->addOperation($opId, $operation);
    }

    return $importer;
  }

}
