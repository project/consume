<?php

namespace Drupal\consume\Import\Persist;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;

/**
 * Writes the import record to the data target.
 */
interface WriterInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Get a short description for the destination of the writer.
   *
   * @return string
   *   A string that identifies the write destination of the writer.
   */
  public function getDestinationName(): string;

  /**
   * Initialize the writer in preparation of writing batches.
   *
   * This process can prepare the target data destination for writing. This can
   * also be some clean-up or tracking updates.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer to which the import operation belongs.
   * @param \Drupal\consume\Import\ImportOperationInterface $operation
   *   The import operation that utilizes this writer instance.
   *
   * @return self
   *   Return this instance for method chaining.
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self;

  /**
   * Create a new instance with the data provided.
   *
   * @param mixed[] $data
   *   The data to be written.
   *
   * @return mixed
   *   The object written.
   */
  public function createEntry(array $data);

  /**
   * Update an existing data item using the data provided.
   *
   * @param mixed[] $data
   *   The data to be written.
   * @param mixed $item
   *   The content object to be updated with the provided data.
   *
   * @return mixed
   *   The object written, if available.
   */
  public function updateEntry(array $data, $item);

  /**
   * Actually writes all the queued data.
   *
   * For efficiency, some writers may queue records instead of writing them
   * immediately. There maybe a lot of different reasons for this.
   *
   * Possible reasons are:
   *  - Gather records for bulk insert.
   *  - Each row / record is only a part of the whole (aggregate records).
   */
  public function flush(): void;

}
