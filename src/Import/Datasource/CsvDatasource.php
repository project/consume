<?php

namespace Drupal\consume\Import\Datasource;

use Drupal\consume\Import\Exception\InvalidDataRowException;
use Drupal\consume\Import\Exception\InvalidDataSourceException;

/**
 * Datasource to iterate and read CSV data entries.
 */
class CsvDatasource implements DatasourceInterface {

  /**
   * File resource handle for open file, when file has been opened.
   *
   * @var int
   */
  private $dataSrc;

  /**
   * The original file object passed set for parsing.
   *
   * @var object|string
   */
  protected $file;

  /**
   * Full filepath, derived from the $this->file.
   *
   * @var string
   */
  protected $filepath;

  /**
   * Current key, the record # (line).
   *
   * @var int
   */
  protected $key;

  /**
   * The column key identifiers for each row of CSV data.
   *
   * @var string[]|null
   */
  protected $keys;

  /**
   * Currently loaded data record.
   *
   * @var array
   */
  protected $cur;

  /**
   * Indicates if this parser looks for the header on the first line.
   *
   * Some CSV files will have the column headers on the first line of
   * file. These are not data, but in fact just the column names
   * that can be used in mapping, and will make each record an associative
   * array instead of just a numerically indexed array.
   *
   * @var bool
   */
  public $hasHeader = TRUE;

  /**
   * Indicates if the character case should be ignored for the value keys.
   *
   * When this is set, all keys will be set to all lowercase. This can be
   * helpful when capitalization of the header / column names is inconsistent
   * and needs to be transformed to all lowercase.
   *
   * @var bool
   */
  public $keysIgnoreCase = FALSE;

  /**
   * Number of lines to skip before reading the file for CSV data content.
   *
   * @var int
   */
  public $skipLines = 0;

  /**
   * The $length parameter for fgetcsv.
   *
   * @var int
   */
  protected $lineLength = 0;

  /**
   * The string delimiter to use separate columns of data.
   *
   * @var string
   */
  public $delimiter = ',';

  /**
   * The character that is used to indicate quoted text.
   *
   * Quoted text will allow text to contain the delimiter character,
   * newlines and other characters not normally allowed in CSV files.
   *
   * @var string
   */
  public $enclosure = '"';

  /**
   * The escape character to use when parsing the CSV.
   *
   * The escape character by default for most spreadsheet
   * applications is the double quotes character.
   *
   * @var string
   */
  public $escape = '"';

  /**
   * Create a new CsvDatasource instance with the proper parsing characters.
   *
   * @param array $header_options
   *   Options on how to handle the CSV header if the first line.
   * @param string $delimiter
   *   The delimiter character to use when separate columns.
   * @param string $enclosure
   *   The character that starts and closes an enclosure (AKA quoted text).
   * @param string $escape
   *   The character that is used for an escape character.
   */
  public function __construct(array $header_options = [], $delimiter = ',', $enclosure = '"', $escape = '"') {
    $this->hasHeader = $header_options['has_header'] ?? TRUE;
    $this->keysIgnoreCase = $header_options['ignore_case'] ?? FALSE;
    $this->delimiter = $delimiter;
    $this->enclosure = $enclosure;
    $this->escape = $escape;
  }

  /**
   * Close the file handle and just serialize the file information.
   *
   * @return array
   *   List of class fields that need to persist through sleep.
   */
  public function __sleep() {
    if (!empty($this->dataSrc)) {
      @fclose($this->dataSrc);
    }

    $this->dataSrc = NULL;

    // Get the list of all defined variables for storage.
    return array_keys(get_object_vars($this));
  }

  /**
   * Clean-up the file handle when the parser is closed.
   *
   * Mostly used if handling a temporary file in a batch, we can
   * clean up the file after we're done.
   */
  public function __destruct() {
    if (!empty($this->dataSrc)) {
      @fclose($this->dataSrc);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function isValidSource($file): bool {
    $path = is_object($file) ? $file->uri->value : $file;

    return is_readable($path);
  }

  /**
   * Read a single line from the CSV file, using the configured settings.
   *
   * Helper function that will call fgetcsv using the correct delimiter,
   * escape, and enclosure settings.
   *
   * @return array|bool
   *   The next line of the source file. Will return a boolean false if it
   *   `fgetcsv` is unable to get another record from the datasource.
   *
   * @see \fgetcsv()
   */
  protected function readLine() {
    $values = fgetcsv($this->dataSrc, NULL, $this->delimiter, $this->enclosure, $this->escape);
    if (!empty($values)) {
      $values = array_map('trim', $values);
    }
    return $values;
  }

  /**
   * Open the source file for reading.
   *
   * @throws \InvalidArgumentException
   */
  protected function openFile(): void {
    if (empty($this->filepath) || ($this->dataSrc = @fopen($this->filepath, 'r')) === FALSE) {
      $msg = "Unable to open file for reading: $this->filepath.";
      throw new InvalidDataSourceException($msg);
    }
  }

  /**
   * Get an identifiable name for the file being parsed.
   *
   * @return string
   *   Gets just the file name, without exposing the full path.
   */
  public function getSourceName(): string {
    return basename($this->filepath);
  }

  /**
   * {@inheritdoc}
   */
  public function setSource($file): void {
    $this->key = -1;
    $this->cur = FALSE;
    $this->keys = NULL;
    $this->file = $file;

    // Keep the raw path for reporting, but use the "real" path.
    $this->filepath = is_object($file) ? $file->uri->value : $file;
    $this->openFile();
  }

  /**
   * {@inheritdoc}
   */
  public function getSource(): mixed {
    return $this->filepath;
  }

  /**
   * {@inheritdoc}
   */
  public function isComplete(): bool {
    return feof($this->dataSrc);
  }

  /**
   * {@inheritdoc}
   */
  public function percentComplete(): float {
    return ftell($this->dataSrc) / filesize($this->filepath);
  }

  /**
   * {@inheritdoc}
   */
  public function getProgress(): array {
    return [
      'key' => $this->key,
      'filename' => $this->file,
      'byte_offset' => ftell($this->dataSrc),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resumeProgress(array $progress): void {
    if ($progress['byte_offset'] !== FALSE && $progress['byte_offset'] > 0) {
      if ($progress['filename'] == $this->file) {
        if (empty($this->dataSrc)) {
          $this->openFile();
        }

        fseek($this->dataSrc, $progress['byte_offset'], SEEK_SET);
        $this->key = $progress['key'];
        $this->next();
      }
      else {
        // We are trying to resume with the wrong file.
        $exceptionMsg = "File being parsed ($this->file) does not match information in the progress file ({$progress['filename']})";
        throw new \InvalidArgumentException($exceptionMsg);
      }
    }
    else {
      $this->rewind();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getKeys(): ?array {
    if (!$this->hasHeader) {
      return NULL;
    }

    if (empty($this->keys)) {
      if (!isset($this->dataSrc)) {
        $this->openFile();
      }

      // Keys are at the start of the file. Save current
      // position, go to the beginning and come back.
      $pos = ftell($this->dataSrc);
      if (fseek($this->dataSrc, 0, SEEK_SET) === 0) {
        if ($this->skipLines > 0) {
          for ($i = 0; $i < $this->skipLines; ++$i) {
            fgets($this->dataSrc);
          }
        }

        $this->keys = $this->readLine();

        if ($this->keys === FALSE) {
          $exceptionMsg = 'File does not contain a header.';
          throw new \UnexpectedValueException($exceptionMsg);
        }

        // Go back to previous location, before method was called.
        fseek($this->dataSrc, $pos, SEEK_SET);
      }

      // Always make sure that we are returning an array.
      if (!is_array($this->keys)) {
        $this->keys = [];
      }

      // Remove any invalid UTF-8 or invisible multibyte characters.
      foreach ($this->keys as &$key) {
        $key = mb_ereg_replace('^\W*([\s\S]*\S)?\W*$', '\1', $key);

        if ($this->keysIgnoreCase && is_string($key)) {
          $key = mb_strtolower($key);
        }
      }
      unset($key);
    }

    return $this->keys;
  }

  /**
   * {@inheritdoc}
   *
   * @see Iterator::rewind()
   */
  public function rewind(): void {
    if (!isset($this->dataSrc)) {
      $this->openFile();
    }

    if (fseek($this->dataSrc, 0, SEEK_SET) === 0) {
      if ($this->skipLines > 0) {
        for ($i = 0; $i < $this->skipLines; ++$i) {
          fgets($this->dataSrc);
        }
      }

      // If there is a header, move to the 2nd line as a starting point.
      if ($this->hasHeader) {
        fgets($this->dataSrc);

        if (empty($this->keys)) {
          $this->getKeys();
        }
      }

      $this->key = -1;
      $this->next();
    }
  }

  /**
   * Returns the next record of data.
   *
   * Next will look for the next non-empty line in the CSV to return.
   * Empty lines will be skipped, and returns NULL if at the end of file.
   *
   * @see Iterator::next()
   */
  public function next(): void {
    $this->cur = FALSE;

    do {
      // Do a line count, even on empty lines (accurate to file).
      ++$this->key;

      if (($values = $this->readLine()) !== FALSE) {
        // Check for blank lines. The fgetcsv() function returns null in the
        // first value of the array.
        if (count($values) > 1 || $values[0] != NULL) {
          if ($this->hasHeader) {
            $keys = $this->getKeys();

            if (count($keys) < count($values)) {
              $error = sprintf('The CSV values for line %d does not have the corrent number of columns of data. Has %d columns when %d was expected', $this->key + 2, count($values), count($keys));
              throw new InvalidDataRowException($error);
            }

            $this->cur = array_combine($keys, $values);
          }
          else {
            $this->cur = $values;
          }

          // We found our first non-empty line, we can return it.
          return;
        }
      }
    } while (!feof($this->dataSrc));
  }

  /**
   * {@inheritdoc}
   *
   * @see Iterator::key()
   */
  #[\ReturnTypeWillChange]
  public function key() {
    return $this->key;
  }

  /**
   * {@inheritdoc}
   *
   * @see Iterator::current()
   */
  #[\ReturnTypeWillChange]
  public function current() {
    return $this->cur;
  }

  /**
   * {@inheritdoc}
   *
   * @see Iterator::valid()
   */
  public function valid(): bool {
    return ($this->cur !== FALSE && is_array($this->cur));
  }

}
