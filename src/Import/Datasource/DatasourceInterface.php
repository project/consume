<?php

namespace Drupal\consume\Import\Datasource;

/**
 * Defines an interface for parsing and traversing data records.
 *
 * A datasource is responsible for extracting data from a data source.
 * The Datasource interface extends the Iterator interface, so it
 * can be traversed and used easily within loops.
 */
interface DatasourceInterface extends \Iterator {

  /**
   * Ensure that data source is usable by and valid.
   *
   * @param mixed $src
   *   Defines the data source that is being use by the datasource.
   *
   * @return bool
   *   Boolean that is TRUE if the source validates for this parser.
   */
  public static function isValidSource($src): bool;

  /**
   * Get a human readable name for the datasource being provided.
   *
   * @return string
   *   A short name or description to identify the data being parsed.
   */
  public function getSourceName(): string;

  /**
   * Helper function to set the current source info for this datasource.
   *
   * The source does get checked internally with static::isValidSource(),
   * it will throw an exception if this validation fails. Use
   * static::isValidSource() directly to avoid the exception.
   *
   * @param mixed $src
   *   The data source information required by this datasource to fetch entries.
   */
  public function setSource($src): void;

  /**
   * Get information about the current datasource.
   *
   * @return mixed
   *   Return information pertaining to the datasource.
   */
  public function getSource(): mixed;

  /**
   * Report the known progress of the datasource (current relative to total).
   *
   * @return float
   *   A number between 0 and 1, that help indicate the progress of the
   *   task. 0 represents that the task is just starting and 1 or more
   *   mean that the datasource traversal is complete.
   */
  public function percentComplete(): float;

  /**
   * Reports if the datasource is at the end of its traversal.
   *
   * @return bool
   *   Is the datasource traversal complete?
   */
  public function isComplete(): bool;

  /**
   * Get the current progress of the datasource traversal.
   *
   * @return array
   *   Get an array of values which provide enough information for
   *   the same type of parser to resume the progress of traversal.
   */
  public function getProgress(): array;

  /**
   * Set the datasource to resume traversal at specified by position.
   *
   * @param array $progress
   *   Information that can be used to resume the progress of the datasource
   *   traversal.
   */
  public function resumeProgress(array $progress): void;

  /**
   * Get an array of column key names.
   *
   * Get the names and the ordering of the data columns returned by the
   * datasource. Not all datasources support this and will return NULL if
   * they do not.
   *
   * @return string[]|null
   *   An array of key values if this datasource supports keys, otherwise NULL.
   */
  public function getKeys(): ?array;

}
