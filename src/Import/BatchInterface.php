<?php

namespace Drupal\consume\Import;

use Drupal\consume\ConsumerInterface;

/**
 * Interface for defining and building batched operations.
 */
interface BatchInterface extends ImporterInterface {

  /**
   * Get a short description of the batch progress.
   *
   * Create a progress message with placeholders to display as the batch is
   * running. Available placeholders are '@current', '@remaining', '@total',
   * '@percentage', '@estimate' and '@elapsed'.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface|string
   *   Object containing translatable string for the batch description.
   */
  public function getProgressMessage();

  /**
   * Get the number items the batch expected to run per batch thread.
   *
   * @return int
   *   The number of items that should run in a batch run. A value of
   *   -1 is used to indicate that the batch expects to run to completion.
   */
  public function getBatchSize(): int;

  /**
   * Fetch the full batch definition as formatted for use with batch_set().
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer executable context for the import operations.
   * @param string[]|null $op_names
   *   The importer operations to run, if NULL or empty, all available
   *   operations will be run.
   *
   * @return array
   *   An array representing the batch information in the appropriate format
   *   for use with batch_set().
   *
   * @see batch_set()
   */
  public function buildBatch(ConsumerInterface $consumer, ?array $op_names = NULL): array;

  /**
   * Batch finish method, used to report the results of the batch operation.
   *
   * @param bool $success
   *   Boolean to indicate if the batch operation was successful.
   * @param array $results
   *   An array of result data stored during the operations.
   * @param array $operations
   *   Array of remaining batch operations if the batch did not complete.
   */
  public function batchFinish($success, array $results, array $operations): void;

}
