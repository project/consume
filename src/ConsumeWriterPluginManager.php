<?php

namespace Drupal\consume;

use Drupal\consume\Annotation\ConsumeWriter;
use Drupal\consume\Import\Persist\WriterInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for import writers.
 */
class ConsumeWriterPluginManager extends DefaultPluginManager {

  /**
   * Constructs a import ConsumeWriterPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Consume/Writer',
      $namespaces,
      $module_handler,
      WriterInterface::class,
      ConsumeWriter::class
    );

    $this->setCacheBackend($cache_backend, 'consume_writer');
  }

}
