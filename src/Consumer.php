<?php

namespace Drupal\consume;

use Drupal\consume\Import\BatchInterface;
use Drupal\consume\Import\Datasource\DatasourceInterface;
use Drupal\consume\Import\Error\ErrorHandlerInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Tracker\HashingTrackerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\UserSession;

/**
 * The executable importer context to consume datasources or data feeds.
 */
class Consumer implements ConsumerInterface {

  use DependencySerializationTrait;

  /**
   * The account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface|null
   */
  protected ?AccountSwitcherInterface $accountSwitcher;

  /**
   * The error handler to capture and log errors.
   *
   * @var \Drupal\consume\Import\Error\ErrorHandlerInterface|null
   */
  protected ?ErrorHandlerInterface $errorHandler;

  /**
   * The user ID to run the import operation as.
   *
   * @var int|null
   */
  protected ?int $importUserId;

  /**
   * An array of datasources to use when executing the import process.
   *
   * @var \Drupal\consume\Import\Datasource\DatasourceInterface[]
   */
  protected array $datasources;

  /**
   * The importer to use when executing this consumer.
   *
   * @var \Drupal\consume\Import\ImporterInterface
   */
  protected ImporterInterface $importer;

  /**
   * Creates a new instance of Consumer class.
   *
   * @param \Drupal\consume\Import\ImporterInterface $importer
   *   The importer this consumer wraps.
   * @param \Drupal\Core\Session\AccountSwitcherInterface|null $account_switcher
   *   Optionally the account switcher service for switching import users.
   */
  public function __construct(ImporterInterface $importer, AccountSwitcherInterface $account_switcher = NULL) {
    $this->importer = $importer;
    $this->accountSwitcher = $account_switcher;
  }

  /**
   * {@inheritdoc}
   */
  public function isBatchable(): bool {
    return $this->importer instanceof BatchInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function hasTrackingChecksum(): bool {
    $operations = $this->importer->getOperations();

    foreach ($operations as $op) {
      if ($op instanceof ImportOperationInterface) {
        $tracker = $op->getTracker();

        if ($tracker instanceof HashingTrackerInterface && $tracker->allowHash()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatasource(string $op_name): ?DatasourceInterface {
    return $this->datasources[$op_name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDatasources(array $datasources): self {
    $this->datasources = $datasources;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImporter(): ImporterInterface {
    return $this->importer;
  }

  /**
   * Get the account switcher.
   *
   * @return \Drupal\Core\Session\AccountSwitcherInterface
   *   The account switcher service.
   */
  protected function getAccountSwitcher(): AccountSwitcherInterface {
    if (!isset($this->accountSwitcher)) {
      $this->accountSwitcher = \Drupal::service('account_switcher');
    }
    return $this->accountSwitcher;
  }

  /**
   * {@inheritdoc}
   */
  public function setImportUser(?int $uid): ConsumerInterface {
    $this->importUserId = $uid;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function ensureUserContext(): self {
    if (isset($this->importUserId)) {
      $session = new UserSession(['uid' => $this->importUserId]);
      $this->getAccountSwitcher()->switchTo($session);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function resetUserContext(): self {
    if (isset($this->importUserId)) {
      try {
        $this->getAccountSwitcher()->switchBack();
      }
      catch (\RuntimeException $e) {
        // Account stack was empty.
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorHandler(): ErrorHandlerInterface {
    return $this->errorHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function setErrorHandler(ErrorHandlerInterface $error_handler): self {
    $this->errorHandler = $error_handler;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function finalize(): void {
    $this->getImporter()->finalize();
    $this->getErrorHandler()->finalize();
  }

}
