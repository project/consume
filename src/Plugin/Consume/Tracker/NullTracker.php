<?php

namespace Drupal\consume\Plugin\Consume\Tracker;

use Drupal\consume\Import\Tracker\TrackerBase;
use Drupal\consume\Import\Tracker\TrackerInterface;

/**
 * A null implementation of an import tracker.
 *
 * Use this tracker when you do not need to update any tracking info on data
 * during an import operation. Data accumulators for example normally do
 * not need to be tracked.
 *
 * @ConsumeTracker(
 *   id = "null_tracker",
 * )
 */
class NullTracker extends TrackerBase implements TrackerInterface {

  /**
   * {@inheritdoc}
   */
  public function getDataId(array $data, array $mappers = []) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType($object): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId($object): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getItem(array &$info) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function allowHash(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchExisting(array $data, array $mappers = []): ?array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshBatchId(array $data, array $mappers = []): void {
  }

  /**
   * {@inheritdoc}
   */
  public function updateTracking(array $data, $item, array $tracking = []): void {
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $ids): void {
  }

  /**
   * {@inheritdoc}
   */
  public function flush(): void {
  }

}
