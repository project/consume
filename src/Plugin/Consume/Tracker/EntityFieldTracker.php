<?php

namespace Drupal\consume\Plugin\Consume\Tracker;

use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Tracker\TrackerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create an import tracker that matches entity fields to data values.
 *
 * @ConsumeTracker(
 *   id = "entity_field_tracker"
 * )
 */
class EntityFieldTracker extends TrackerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The database specifications for finding an entity by field value.
   *
   * @var array|null
   */
  protected $fieldSqlInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    // Add references to the entity type, and entity field managers.
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self {
    parent::setup($importer, $operation);

    ['field_name' => $fieldName, 'entity_type' => $entityType] = $this->configuration;
    $fieldDef = $this->entityFieldManager->getFieldStorageDefinitions($entityType)[$fieldName] ?? NULL;

    if ($fieldDef) {
      /** @var \Drupal\Core\Entity\Sql\SqlEntityStorageInterface $storage */
      $storage = $this->entityTypeManager->getStorage($entityType);
      /** @var \Drupal\Core\Entity\Sql\TableMappingInterface $schema */
      $schema = $storage->getTableMapping();

      $property = $this->configuration['property'] ?? $fieldDef->getMainPropertyName();
      $this->fieldSqlInfo = [
        'table' => $schema->getFieldTableName($fieldName),
        'column' => $schema->getFieldColumnName($fieldDef, $property),
      ];
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataId(array $data, array $mappers = []) {
    $dataCol = $this->configuration['field_name'];

    if (!empty($mappers[$dataCol])) {
      $mapped = [];
      $mappers[$dataCol]->map($data, $mapped);
      return $mapped[$dataCol] ?? NULL;
    }

    return $data[$dataCol] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType($object): string {
    if ($object instanceof ContentEntityInterface) {
      return $object->getEntityTypeId();
    }

    $err = sprintf('Entity tracker requires a content entity. An object of type %s was found instead.', get_class($object));
    throw new \InvalidArgumentException($err);
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId($object): string {
    if ($object instanceof ContentEntityInterface) {
      return $object->id();
    }

    $err = sprintf('Entity tracker requires a content entity. An object of type %s was found instead.', get_class($object));
    throw new \InvalidArgumentException($err);
  }

  /**
   * {@inheritdoc}
   */
  public function getItem(array &$info) {
    if (isset($info['item'])) {
      return $info['item'] ?: FALSE;
    }
    elseif (!empty($info['item_id']) && !empty($info['type'])) {
      $entity = $this->entityTypeManager
        ->getStorage($info['type'])
        ->load($info['item_id']);

      if ($entity) {
        $info['item'] = $entity;
        return $entity;
      }
    }

    $info['item'] = FALSE;
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchExisting(array $data, array $mappers = []): ?array {
    $dataId = $this->getDataId($data, $mappers);

    // Check if this item has been imported previously and is already
    // being tracked by the import process.
    $tracked = $this->db
      ->select($this->getTable(), 'track')
      ->fields('track')
      ->condition('import_name', $this->importName)
      ->condition('id', $dataId)
      ->execute()
      ->fetchAssoc();

    if (!$tracked && !empty($this->fieldSqlInfo)) {
      $entityId = $this->db
        ->select($this->fieldSqlInfo['table'], 'field')
        ->fields('field', ['entity_id'])
        ->condition($this->fieldSqlInfo['column'], $dataId)
        ->range(0, 1)
        ->execute()
        ->fetchField();

      if ($entityId) {
        $entityType = $this->configuration['entity_type'];
        return [
          'import_name' => $this->importName,
          'id' => $dataId,
          'type' => $entityType,
          'item_id' => $entityId,
        ];
      }
    }

    return $tracked ?: NULL;
  }

}
