<?php

namespace Drupal\consume\Plugin\Consume\Writer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Persist\WriterInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Import writer which accumulates serialized data to a storage.
 *
 * This importer is useful for data imports that accumulate the data from
 * multiple datasource or files. This allows each pass to "accumulate" data
 * on each pass and keep it for later use.
 *
 * Often this writer is combined with the
 * \Drupal\consume\Process\ApplyAccumulatedProcessor in order to fetch the
 * stored data and apply it to the mapped data to be imported.
 *
 * @see \Drupal\consume\Process\ApplyAccumulatedProcessor
 *
 * @ConsumeWriter(
 *   id = "data_accumulator",
 *   label = @Translation("Data accumulator"),
 * )
 */
class DataAccumulator extends PluginBase implements WriterInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * Import storage name.
   *
   * @var string
   */
  protected $importName;

  /**
   * Database connection to make queries and store the data.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $db;

  /**
   * The content serializer to use with the data accumulator.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * A write buffer for accumulating data.
   *
   * Often times the accumulator is used in circumstances where there is a
   * many to one relationship and can benefit from delaying the writing
   * to the database while it collects and consolidates rows.
   *
   * @var array
   */
  protected array $buffer = [];

  /**
   * Creates a new instance of the DataAccumulator import writer plugin.
   *
   * @param array $configuration
   *   The writer plugin configuration.
   * @param string $plugin_id
   *   The plugin ID for this writer.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to make database queries against.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The data serializer to use when encoding data for storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, SerializationInterface $serializer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->db = $database;
    $this->serializer = $serializer;

    // Apply default values for the accumulator behaviors.
    $this->configuration += $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('serialization.json')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'import_name' => NULL,
      'table' => 'consume_data_accumulator',
      'initialize_empty' => FALSE,
      'merge_method' => 'mergeShallow',
      'merge_deep_keys' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationName(): string {
    return 'data accumulator';
  }

  /**
   * {@inheritdoc}
   */
  public function getTable(): string {
    return $this->configuration['table'];
  }

  /**
   * Merge the incoming data with any existing.
   *
   * The merge method is defined in the configuration (option: 'merge_method').
   *
   * @param array $data
   *   The incoming data to merge with existing data.
   * @param array $mergeData
   *   The existing data to merge into the current data value.
   *
   * @return array
   *   The merged data values.
   */
  protected function mergeData(array $data, array $mergeData): array {
    $method = $this->configuration['merge_method'] ?? 'mergeShallow';
    return $this->{$method}($data, $mergeData);
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self {
    $this->importName = $this->configuration['import_name'] ?: $importer->id();

    // This only needs to be set for the first operation that has a
    // data accumulator to clear it for this import name or ID.
    // Reminder that all operation::init() methods are called during the
    // importer::init() even if the operator doesn't get invoked or called.
    if (!empty($this->configuration['initialize_empty'])) {
      $this->db->delete($this->getTable())
        ->condition('import_name', $this->importName)
        ->execute();
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntry(array $data) {
    if ($id = $data[$this->configuration['id']]) {
      $this->buffer[$id] = isset($this->buffer[$id]) && is_array($this->buffer[$id])
        ? $this->mergeData($data, $this->buffer[$id])
        : $data;

      return $this->buffer[$id];
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntry(array $data, $item) {
    // Use the buffer so any accumulated data can be aggregated.
    // The existing data will be fetch and written in the static::flush() method
    // more consistently if the item was already available in the database.
    //
    // Data accumulators are most commonly used with "null_trackers" anyways?
    return $this->createEntry($data);
  }

  /**
   * {@inheritdoc}
   */
  public function flush(): void {
    if ($this->buffer) {
      $ids = array_keys($this->buffer);
      $table = $this->getTable();

      $existing = $this->db->select($table, 'acc')
        ->fields('acc', ['id', 'data'])
        ->condition('import_name', $this->importName)
        ->condition('id', $ids, 'IN')
        ->execute()
        ->fetchAllKeyed();

      foreach ($existing as $id => $value) {
        // Empty values should never get added to the accumulator but just in
        // case, it would mean that the current DB data should remain unchanged.
        if (!empty($this->buffer[$id])) {
          $old = $this->serializer->decode($value);
          $value = is_array($old)
            ? $this->mergeData($this->buffer[$id], $old)
            : $this->buffer[$id];

          $this->db->update($table)
            ->fields(['data' => $this->serializer->encode($value)])
            ->condition('import_name', $this->importName)
            ->condition('id', $id)
            ->execute();
        }
        unset($this->buffer[$id]);
      }

      // Any items left to be newly written?
      if ($this->buffer) {
        $insertQry = $this->db->insert($table)->fields([
          'import_name',
          'id',
          'data',
        ]);

        $hasData = FALSE;
        foreach ($this->buffer as $id => $data) {
          if ($data) {
            $insertQry->values([
              $this->importName,
              $id,
              $this->serializer->encode($data),
            ]);
            $hasData = TRUE;
          }
        }

        if ($hasData) {
          $insertQry->execute();
        }
      }
    }

    // Reset the buffer, these items have been written.
    $this->buffer = [];
  }

  /**
   * Overwrite existing data, and always use the latest incoming values.
   *
   * @param array $data
   *   The incoming data to be added to the accumulated data storage.
   * @param array $existing
   *   The previously accumulated data.
   *
   * @return array
   *   The resulting data from the data merge.
   */
  public function overwrite(array $data, array $existing): array {
    return $data;
  }

  /**
   * All newly defined values take precedence over existing data.
   *
   * This means that any data keys and fields defined in the incoming data
   * will overwrite data found in the existing data.
   *
   * @param array $data
   *   The incoming data to be added to the accumulated data storage.
   * @param array $existing
   *   The previously accumulated data.
   *
   * @return array
   *   The resulting data from the data merge.
   */
  public function mergeShallow(array $data, array $existing): array {
    return $data + $existing;
  }

  /**
   * Merge all data into existing data recursively.
   *
   * @param array $data
   *   The incoming data to be added to the accumulated data storage.
   * @param array $existing
   *   The previously accumulated data.
   *
   * @return array
   *   The resulting data from the data merge.
   */
  public function mergeDeep(array $data, array $existing): array {
    $massageValues = function (&$value) {
      if (is_array($value)) {
        $value = array_filter(array_unique($value), function ($a) {
          return $a !== NULL && (!is_array($a) || $a);
        });
      }
    };

    // Only merge these keys recursively, otherwise merge all values.
    if (!empty($this->configuration['merge_deep_keys'])) {
      $values = [];

      foreach ($this->configuration['merge_deep_keys'] as $key) {
        if (empty($data[$key])) {
          $values[$key] = $existing[$key] ?? NULL;
        }
        elseif (empty($existing[$key])) {
          $values[$key] = $data[$key];
        }
        elseif (is_array($data[$key])) {
          if (is_array($existing[$key])) {
            $values[$key] = array_merge_recursive($data[$key], $existing[$key]);
          }
          else {
            $values[$key] = $data[$key];
            $values[$key][] = $existing[$key];
          }
        }
        elseif (is_array($existing[$key])) {
          $values[$key] = $existing[$key];
          $values[$key][] = $data[$key];
        }
        else {
          // Neither are an array, make them array with both values.
          $values[$key] = [$data[$key], $existing[$key]];
        }
        $massageValues($values[$key]);
      }

      $values += $data + $existing;
    }
    else {
      $values = array_merge_recursive($data, $existing);
      array_walk($values, $massageValues);
    }

    return $values;
  }

}
