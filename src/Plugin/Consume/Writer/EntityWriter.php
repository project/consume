<?php

namespace Drupal\consume\Plugin\Consume\Writer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\consume\Import\Exception\AggregatedImportErrors;
use Drupal\consume\Import\Exception\ResumeableException;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Persist\WriterInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity data writer for creating new entities from data.
 *
 * @ConsumeWriter(
 *   id = "entity_writer",
 *   label = @Translation("Entity writer"),
 * )
 */
class EntityWriter extends PluginBase implements WriterInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * Entity type manager service for Drupal.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service to use for getting field definitions.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Defaults to use when generating new entities.
   *
   * @var array
   */
  protected $entityDefaults;

  /**
   * Construct a new writer object, for writing the hearings records.
   *
   * @param array $configuration
   *   The configuration for the entity writer. Needs to include entity_type
   *   and bundle information.
   * @param string $plugin_id
   *   The ID of the plugin.
   * @param mixed $plugin_definition
   *   Plugin definition from the plugin discovery.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The service manager for getting services related to entity types.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   Entity field manager object to fetch field properties.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->configuration += $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_type' => NULL,
      'entity_defaults' => [],
      'no_update' => [],
      'update_only' => [],
      'use_field_defaults' => [],
      'skip_validation' => [],
      'publish_entity' => TRUE,
      'allow_create' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationName(): string {
    $entityType = $this->configuration['entity_type'];

    return $this->entityTypeManager
      ->getDefinition($entityType)
      ->get('label');
  }

  /**
   * {@inheritdoc}
   */
  public function setup(ImporterInterface $importer, ImportOperationInterface $operation): self {
    $entityType = $this->configuration['entity_type'];
    $entityDef = $this->entityTypeManager->getDefinition($entityType);

    $this->entityDefaults = $this->configuration['entity_defaults'];
    $this->entityDefaults += ['uid' => 1];

    foreach (['bundle', 'status', 'langcode'] as $key) {
      $bundleKey = $entityDef->getKey($key);

      if ($bundleKey && $key != $bundleKey && !empty($this->entityDefaults[$key])) {
        $this->entityDefaults[$bundleKey] = $this->entityDefaults[$key];
        unset($this->entityDefaults[$key]);
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createEntry(array $data) {
    if (empty($this->configuration['allow_create'])) {
      // No new entity creation allowed for this entity.
      $error = sprintf('Entity creation of %s type is not allowed for writer of %s.', $this->configuration['entity_type'], $this->getDestinationName());
      throw new ResumeableException($error);
    }

    $entityType = $this->configuration['entity_type'];
    $entity = $this->entityTypeManager
      ->getStorage($entityType)
      ->create($this->entityDefaults);

    $this->write($data, $entity);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function updateEntry(array $data, $item) {
    $this->write($data, $item);
    return $item;
  }

  /**
   * Applies the data values to the content entities and saves the changes.
   *
   * @param array $data
   *   The data to write to the entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being written to.
   *
   * @return int
   *   Returns the result of the ContentEntityInterface::save() method, which
   *   will indicate if the entity was saved or updated.
   *
   * @see \Drupal\Core\Entity\ContentEntityInterface::save()
   */
  public function write(array $data, ContentEntityInterface $entity): int {
    if (empty($data)) {
      throw new \InvalidArgumentException('Entity writer was unable to create entity from empty data.');
    }

    $config = $this->configuration;
    $fieldErrors = [];

    foreach ($data as $name => $value) {
      try {
        if ($entity->hasField($name)) {
          /** @var \Drupal\Core\Field\FieldItemListInterface $itemList */
          $itemList = $entity->{$name};

          // If field is configured for no updates, and is already populated,
          // then skip setting the value and leave the existing value intacted.
          if (!empty($config['no_update'][$name]) && !$itemList->isEmpty()) {
            continue;
          }
          // Skip if the value is empty, and the field is set to "update only".
          elseif (empty($value) && !empty($config['update_only'][$name])) {
            continue;
          }
          $itemList->setValue($value);

          // If empty, check if we should use the field defaults, otherwise
          // check the validation for the set values.
          if (!empty($config['use_field_defaults'][$name]) && $itemList->isEmpty()) {
            $itemList->applyDefaultValue();
          }
          elseif (empty($config['skip_validation'][$name])) {
            $fieldValidation = $itemList->validate();

            if ($fieldValidation->count()) {
              $fieldErrors[$name] = $name . ': ';
              foreach ($fieldValidation as $constraint) {
                $fieldErrors[$name] .= $constraint->getPropertyPath() . '--' . $constraint->getMessage();
              }
            }
          }
        }
      }
      catch (\Exception $e) {
        $fieldErrors[$name] = $name . ': ' . Error::renderExceptionSafe($e);
      }
    }

    if ($fieldErrors) {
      throw new AggregatedImportErrors($fieldErrors);
    }

    // Ensure that the entity has been published.
    if (!empty($config['publish_entity'])) {
      if ($entity instanceof EntityPublishedInterface) {
        $entity->setPublished();
      }
      else {
        $entityType = $this->configuration['entity_type'];
        $entityDef = $this->entityTypeManager->getDefinition($entityType);

        if ($entityDef->hasKey('status')) {
          $entity->set($entityDef->getKey('status'), TRUE);
        }
      }
    }

    return $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function flush(): void {
    // Entities were written immediately so nothing needs to happen here.
  }

}
