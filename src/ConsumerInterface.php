<?php

namespace Drupal\consume;

use Drupal\consume\Import\Datasource\DatasourceInterface;
use Drupal\consume\Import\Error\ErrorHandlerInterface;
use Drupal\consume\Import\ImporterInterface;

/**
 * The import consumer which is the execution context of an importer.
 */
interface ConsumerInterface {

  /**
   * Is this consumer batchable?
   *
   * @return bool
   *   TRUE if the consumer wraps a batchable importer process.
   */
  public function isBatchable(): bool;

  /**
   * Does this importer process utilize hash checksums?
   *
   * @return bool
   *   TRUE if this import utilizes hash checksums, FALSE otherwise.
   */
  public function hasTrackingChecksum(): bool;

  /**
   * Get the datasource for a requested operation.
   *
   * @param string $op_name
   *   The operation name of the datasource to get.
   *
   * @return \Drupal\consume\Import\Datasource\DatasourceInterface|null
   *   The datasource for the operation requested or NULL if there is no
   *   matching datasource.
   */
  public function getDatasource(string $op_name): ?DatasourceInterface;

  /**
   * Set the datasource to use when executing the consumer import process.
   *
   * @param \Drupal\consume\Import\Datasource\DatasourceInterface[] $datasources
   *   The datasources to use for the import operations.
   *
   * @return self
   *   The consumer instance for method chaining.
   */
  public function setDatasources(array $datasources): self;

  /**
   * Set the user ID to run the import operations as.
   *
   * @param int|null $uid
   *   The user ID of the user to execute the import operations as.
   *
   * @return self
   *   The consumer instance for method chaining.
   */
  public function setImportUser(?int $uid): self;

  /**
   * Get the importer instance wrapped by this consumer.
   *
   * @return \Drupal\consume\Import\ImporterInterface
   *   The importer instance wrapped by this consumer.
   */
  public function getImporter(): ImporterInterface;

  /**
   * Ensure the user context is set at the start of execution.
   *
   * @return self
   *   The consumer instance for method chaining.
   */
  public function ensureUserContext(): self;

  /**
   * Reset the user context and account back.
   *
   * @return self
   *   The consumer instance for method chaining.
   */
  public function resetUserContext(): self;

  /**
   * Get the error handler currently set.
   *
   * @return \Drupal\consume\Import\Error\ErrorHandlerInterface
   *   The error handler currently set for this consumer.
   */
  public function getErrorHandler(): ErrorHandlerInterface;

  /**
   * Set the error handler to use when encountering errors during processing.
   *
   * @param \Drupal\consume\Import\Error\ErrorHandlerInterface $error_handler
   *   The error handler to use when encountering errors while executing an
   *   import process.
   *
   * @return self
   *   The consumer object for method chaining.
   */
  public function setErrorHandler(ErrorHandlerInterface $error_handler): self;

  /**
   * Release any resources and do any clean-up for the consumer.
   *
   * The consumer has completed its processing at this point an should delete
   * any allocations and release resources.
   */
  public function finalize(): void;

}
