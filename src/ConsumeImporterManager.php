<?php

namespace Drupal\consume;

use Drupal\consume\Import\BatchImporter;
use Drupal\consume\Import\ExclusiveBatchImporter;
use Drupal\consume\Import\ImporterFactory;
use Drupal\consume\Import\ImportOperationFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\toolshed\Strategy\StrategyFactoryInterface;
use Drupal\toolshed\Strategy\StrategyManager;

/**
 * Strategy manager for consume importers.
 */
class ConsumeImporterManager extends StrategyManager {

  /**
   * The factory implementation to use when constructing new import operations.
   *
   * @var \Drupal\consume\Import\ImportOperationFactory
   */
  protected $opFactory;

  /**
   * Creates a new instance of the default importer strategy manager.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The discovery cache backend.
   * @param \Drupal\consume\Import\ImportOperationFactory $operation_factory
   *   The import operation factory service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, ImportOperationFactory $operation_factory) {
    parent::__construct('consume', $module_handler, $cache_backend, ['consume:importer']);

    $this->opFactory = $operation_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFactory(): StrategyFactoryInterface {
    if (!isset($this->factory)) {
      $this->factory = new ImporterFactory($this->opFactory);
    }

    return $this->factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultStrategyClass(array $definition): string {
    if (!empty($definition['class'])) {
      return $definition['class'];
    }

    return empty($definition['exclusive'])
      ? BatchImporter::class
      : ExclusiveBatchImporter::class;
  }

}
