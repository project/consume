<?php

namespace Drupal\consume;

/**
 * Notification kill switch object.
 *
 * The notification kill switch maintains an on/off ($suppress) flag that
 * determines if the email message IDs in the $suppressIds property are
 * not sent if the $suppress flag is on.
 *
 * This is meant to be used as a service with request scoping, so needs to be
 * turned per request that needs it. This allows imports to turn it off and
 * not block any notifications on any other threads or requests.
 */
class NotificationKillSwitch implements NotificationKillSwitchInterface {

  /**
   * List of message IDs to suppressed if the kill switch is enabled.
   *
   * @var array
   *
   * @see \Drupal\Core\Mail\MailManagerInterface::mail()
   * @see consume_mail_alter()
   * @see /consume.services.yml
   */
  protected array $suppressIds = [];

  /**
   * Internal state to determine if notification suppression is on.
   *
   * @var bool
   */
  protected bool $suppress = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setSuppressId(array $mail_ids = []): void {
    $this->suppressIds = array_combine($mail_ids, $mail_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function isSuppressing(): bool {
    return $this->suppress;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldSuppress($message_id): bool {
    return $this->suppress && isset($this->suppressIds[$message_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function suppress(): void {
    $this->suppress = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function unsuppress(): void {
    $this->suppress = FALSE;
  }

}
