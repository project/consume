<?php

namespace Drupal\consume\Form;

use Drupal\consume\ConsumeImporterManager;
use Drupal\consume\Consumer;
use Drupal\consume\ConsumerInterface;
use Drupal\consume\Import\BatchInterface;
use Drupal\consume\Import\Error\ErrorHandlerInterface;
use Drupal\consume\Import\Error\LogErrorHandler;
use Drupal\consume\Import\Exception\BatchAlreadyRunningException;
use Drupal\consume\Import\Exception\InvalidDataSourceException;
use Drupal\consume\Import\ExclusiveImporterInterface;
use Drupal\consume\Import\ImporterInterface;
use Drupal\consume\Import\ImportOperationInterface;
use Drupal\consume\Import\Tracker\HashingTrackerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form displaying import summaries and manual triggering of importers.
 *
 * Derived forms of this class should ensure that they are configuring or
 * uploading the datasources and attaching them to the importer.
 */
abstract class ImportFormBase extends FormBase {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The importer manager.
   *
   * @var \Drupal\consume\ConsumeImporterManager
   */
  protected ConsumeImporterManager $importManager;

  /**
   * The importer this form works with if it has been initialized.
   *
   * @var \Drupal\consume\Import\ImporterInterface|null
   */
  protected ?ImporterInterface $importer;

  /**
   * Creates a new instance of the Importer admin form.
   *
   * @param \Drupal\consume\ConsumeImporterManager $import_manager
   *   The importer manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(ConsumeImporterManager $import_manager, DateFormatterInterface $date_formatter) {
    $this->importManager = $import_manager;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('strategy.manager.consume.importer'),
      $container->get('date.formatter')
    );
  }

  /**
   * Gets the Importer ID of the importer this form administers.
   *
   * @return string
   *   The importer ID.
   */
  abstract protected function getImportId(): string;

  /**
   * Get the user ID of the user to run the import process as.
   *
   * @return int|null
   *   The user ID of the user to run the import process as. Returning NULL
   *   means no user context change should happen, or 0 means run as anonymous.
   */
  public function getImportUserId(): ?int {
    return NULL;
  }

  /**
   * Get the error handler that should be used with the consumer execution.
   *
   * @param \Drupal\consume\ConsumerInterface $consumer
   *   The consumer to build the error handler for.
   *
   * @return \Drupal\consume\Import\Error\ErrorHandlerInterface
   *   The import process error handler.
   */
  protected function getErrorHandler(ConsumerInterface $consumer): ErrorHandlerInterface {
    return new LogErrorHandler($consumer);
  }

  /**
   * Attach the datasources to the importer before preparing to run.
   *
   * Normally called as part of the form submit handler, and can get submitted
   * form assets (like file uploads) or form values as the datasources or
   * factors to determine how datasources are connected.
   *
   * @param array $form
   *   Reference to the form elements and structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build info.
   *
   * @return array
   *   An array of datasources to use for the import process. Datasources
   *   should be keyed by the operation name.
   */
  abstract protected function getDatasources(array &$form, FormStateInterface $form_state): array;

  /**
   * Get the operation IDs for all the operations that should be run.
   *
   * This allows an importer to change the operations that get run during the
   * import process run.
   *
   * @param array $form
   *   The form elements and structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form build information, state and values.
   *
   * @return string[]|null
   *   Array of operation names to run. If empty or NULL, then all available
   *   operations will be run.
   */
  protected function getOperationNames(array $form, FormStateInterface $form_state): ?array {
    return NULL;
  }

  /**
   * Get the importer instance to use with this importer form.
   *
   * @return \Drupal\consume\Import\ImporterInterface
   *   The importer instance managed by this form.
   */
  protected function getImporter(): ImporterInterface {
    return $this->importManager->getInstance($this->getImportId());
  }

  /**
   * Format a status value into a renderable for display.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $label
   *   Label to use for the statistic field.
   * @param mixed $value
   *   The value to display.
   *
   * @return array
   *   The renderable to display for this status value.
   */
  protected function formatStatusValue($label, $value): array {
    return [
      '#theme' => 'consume_status_summary_field',
      '#label' => $label,
      '#value' => $value,
    ];
  }

  /**
   * Create a status renderable from a timestamp value.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|string $label
   *   Label to use for the statistic field.
   * @param int $timestamp
   *   The timestamp value to format a date for.
   *
   * @return array
   *   A renderable to display the timestamp value.
   */
  protected function formatStatusTimestamp($label, $timestamp): array {
    if (is_int($timestamp) && $timestamp > 0) {
      $dateStr = $this->dateFormatter->format($timestamp);
      return $this->formatStatusValue($label, $dateStr);
    }

    return $this->formatStatusValue($label, '--');
  }

  /**
   * Does this importer support tracking of items with a hash checksum?
   *
   * @return bool
   *   If this importer has a tracker which supports a hash checksum then TRUE
   *   otherwise return FALSE.
   */
  protected function importHasHashTracking(): bool {
    $operations = $this->getImporter()->getOperations();

    foreach ($operations as $op) {
      if ($op instanceof ImportOperationInterface) {
        $tracker = $op->getTracker();

        if ($tracker instanceof HashingTrackerInterface && $tracker->allowHash()) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Renderable array of the instructions to display for the import actions.
   *
   * @return array
   *   Render array for the help text to display as the text with the import
   *   buttons.
   */
  protected function getImportHelpDescription(): array {
    $markup[] = $this->t('<strong>Run Import:</strong> Manually start the import process. Keep in mind that this process can take several minutes and should not be interupted once it has been started.');

    // Add instructions and description for the clear checksum button.
    if ($this->importHasHashTracking()) {
      $markup[] = $this->t('<strong>Clear Checksum:</strong> This import process keeps a hash checksum to detect changes in the source data. This allows the import to skip importing if the source data has not changed. Clear this information to ensure that the importer attempts to update all content on the next process execution.');
    }

    $build = [];
    foreach ($markup as $content) {
      $build[] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $content,
      ];
    }
    return $build;
  }

  /**
   * Build the summary of the importer status.
   *
   * @todo the display elements of the summary should be constructed by the
   * importer, or even a status info handler. This way importers have control
   * over how the status information is managed and provided.
   *
   * @return array
   *   Renderable array of the importer status summary. Provides information
   *   about the last time the importer process was run.
   */
  protected function buildSummary(): array {
    $importer = $this->getImporter();
    $summary = [
      '#type' => 'fieldset',
      '#title' => $this->t('Summary'),
      '#attributes' => [
        'class' => ['import-status'],
      ],
    ];

    if ($status = $importer->getStatusInfo()) {
      $summary['overview'] = [
        'phase' => $this->formatStatusValue($this->t('Step'), $status['phase']),
        'started' => $this->formatStatusTimestamp($this->t('Started on'), $status['start']),
        'finish' => $this->formatStatusTimestamp($this->t('Completed on'), $status['finish']),
      ];

      foreach ($importer->getOperations() as $name => $op) {
        if (!empty($status['operations'][$name])) {
          $summary[$name] = [
            '#type' => 'fieldset',
            '#title' => $op->getTitle(),
            '#attributes' => [],

            'content' => $op->formatStatus($status['operations'][$name]),
          ];
        }
      }
    }
    else {
      $summary['message']['#markup'] = $this->t('No data about the import process is currently available.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {
      $importer = $this->getImporter();

      $form['summary'] = $this->buildSummary();
      $form['actions'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Import'),

        'message' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => [],
          ],
        ],
      ];
      $message = &$form['actions']['message'];

      // Determine if the batch is locked, and provide the appropriate action
      // for the current importer status.
      if (!($importer instanceof ExclusiveImporterInterface) || $importer->isLockAvailable()) {
        $message['#attributes']['class'][] = 'description';
        $message['content'] = $this->getImportHelpDescription();

        $form['actions']['import'] = [
          '#type' => 'submit',
          '#value' => $this->t('Run Import'),
          '#attributes' => [
            'class' => ['button--primary'],
          ],
        ];

        if ($this->importHasHashTracking()) {
          $form['actions']['clear_hash'] = [
            '#type' => 'submit',
            '#value' => $this->t('Clear Checksum'),
            '#limit_validation_errors' => [['actions']],
            '#submit' => [
              [$this, 'submitClearChecksum'],
            ],
            '#attributes' => [
              'class' => ['button--secondary'],
            ],
          ];
        }
      }
      else {
        $message['#attributes']['class'][] = 'messages';
        $message['#attributes']['class'][] = 'messages--warning';
        $message['content'] = [
          '#markup' => $this->t('This import may already be running or the process may have unexpectedly crashed or exited prematurely. You can wait for the process to complete, or unlock the process by clicking "Release import lock" below.'),
        ];

        $form['actions']['release_lock'] = [
          '#type' => 'submit',
          '#value' => $this->t('Release import lock'),
          '#limit_validation_errors' => [['actions']],
          '#submit' => [
            [$this, 'submitReleaseLock'],
          ],
          '#attributes' => [
            'class' => ['button--danger'],
          ],
        ];
      }
    }
    catch (\Throwable $e) {
      $this
        ->logger('consume.' . $this->getImportId())
        ->error(Error::DEFAULT_ERROR_MESSAGE, Error::decodeException($e));

      $this
        ->messenger()
        ->addError($this->t('Unable to load importer due to errors with the definition. See the administrative logs for more details about the error.'));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $importer = $this->getImporter();
      $importer->setup();
      $consumer = new Consumer($importer);
      $consumer
        ->setImportUser($this->getImportUserId())
        ->setErrorHandler($this->getErrorHandler($consumer))
        ->setDatasources($this->getDatasources($form, $form_state));

      $opNames = $this->getOperationNames($form, $form_state);
      // Run the importer either as a batch or one operation at a time.
      if ($importer instanceof BatchInterface) {
        batch_set($importer->buildBatch($consumer, $opNames));
      }
      else {
        $importer->execute($consumer, $opNames);
        $importer->finalize();
      }
    }
    catch (BatchAlreadyRunningException $e) {
      $this->messenger()->addError('The import process may already be running, if this is the case, the import will become available again when it completes. You can release this lock if you need to.');
    }
    catch (InvalidDataSourceException $e) {
      if (isset($importer)) {
        $importer->finalize();
      }

      $this->messenger()->addError($this->t('Unable to load data: @message', [
        '@message' => $e->getMessage(),
      ]));
    }
    catch (\Throwable $e) {
      if (isset($importer)) {
        $importer->finalize();
      }

      $this
        ->logger('consume.' . $this->getImportId())
        ->error(Error::DEFAULT_ERROR_MESSAGE, Error::decodeException($e));

      $this
        ->messenger()
        ->addError($this->t('Failed to execute the import operations, see the administrative logs for more details.'));
    }
  }

  /**
   * Form submit callback which clears the importer hash checksum.
   *
   * @param array $form
   *   Reference to the form structure and elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build information.
   */
  public function submitClearChecksum(array &$form, FormStateInterface $form_state) {
    $importer = $this->getImporter();

    foreach ($importer->getOperations() as $op) {
      if ($op instanceof ImportOperationInterface) {
        $tracker = $op->getTracker();
        $tracker->setup($importer, $op);

        if ($tracker instanceof HashingTrackerInterface && $tracker->allowHash()) {
          $tracker->clearHash();
        }
      }
    }

    $this->messenger()->addStatus($this->t('Change detection has been cleared. Next import will update all allowed values.'));
  }

  /**
   * Form submit callback to release the importer lock.
   *
   * @param array $form
   *   Reference to the form elements and structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values, and build information.
   */
  public function submitReleaseLock(array &$form, FormStateInterface $form_state) {
    $importer = $this->getImporter();

    if ($importer instanceof ExclusiveImporterInterface) {
      $importer->releaseLock();
    }

    $this->messenger()->addStatus($this->t('Lock on the import process has been released. The should now be available to be manually started!'));
  }

}
