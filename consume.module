<?php

/**
 * @file
 * Drupal hooks and global module functionality for the Consume module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_mail_alter().
 */
function consume_mail_alter(&$message) {
  $notifySwitch = \Drupal::service('consume.notification.kill_switch');

  // Suppress user notification while an import has prevented notifications
  // from being sent. This is service instance is scoped to the current request
  // and therefore should not prevent other requests / instances from sending.
  if ($notifySwitch->shouldSuppress($message['id'])) {
    $message['send'] = FALSE;
  }
}

/**
 * Implements hook_entity_delete().
 */
function consume_entity_delete(EntityInterface $entity) {
  // @todo clean-up tracking data from consume import trackers. This method
  // works for the short term but won't support more complex custom trackers.
  \Drupal::database()
    ->delete('consume_tracker')
    ->condition('type', $entity->getEntityTypeId())
    ->condition('item_id', $entity->id())
    ->execute();
}

/**
 * Implements hook_theme().
 */
function consume_theme() {
  $themes = [];
  $themes['consume_status_summary_field'] = [
    'variables' => [
      'label' => '',
      'value' => '',
    ],
  ];

  return $themes;
}

/**
 * Implements hook_preprocess_consume_status_summary_field().
 */
function template_preprocess_consume_status_summary_field(&$variables) {
  $variables['title_attributes']['class'][] = 'importer__status-label';
  $variables['content_attributes']['class'][] = 'importer__status-value';
}
