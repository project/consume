<?php

/**
 * @file
 * Contains the installation hooks and update functions for the Consume module.
 */

/**
 * Implements hook_schema().
 */
function consume_schema() {
  $schema = [];
  $schema['consume_tracker'] = [
    'description' => 'Default table for tracker indexes.',
    'fields' => [
      'batch_id' => [
        'description' => 'Unique identifier of the batch.',
        'type' => 'char',
        'length' => 48,
        'not null' => TRUE,
      ],
      'import_name' => [
        'description' => 'The machine name of the import operation.',
        'type' => 'varchar_ascii',
        'length' => 128,
        'not null' => TRUE,
      ],
      'id' => [
        'description' => 'Tracker ID for the data record.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
      ],
      'type' => [
        'description' => "Tracked item's type for the data record.",
        'type' => 'varchar_ascii',
        'length' => 128,
      ],
      'item_id' => [
        'description' => 'Tracker ID for the data record.',
        'type' => 'varchar_ascii',
        'length' => 255,
      ],
      'hash' => [
        'description' => 'The hashed of the data record for change detection.',
        'type' => 'varchar_ascii',
        'length' => 80,
      ],
      'date' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'size' => 'big',
        'default' => 0,
      ],
    ],
    'indexes' => [
      'batch' => ['import_name', 'batch_id'],
      'import' => ['import_name', 'id'],
      'ref_item' => ['import_name', 'type', 'item_id'],
    ],
  ];

  $schema['consume_data_accumulator'] = [
    'description' => 'Allows importers to accumulate data across multiple operations.',
    'fields' => [
      'import_name' => [
        'description' => 'The machine name of the import operation.',
        'type' => 'varchar_ascii',
        'length' => 128,
        'not null' => TRUE,
      ],
      'id' => [
        'description' => 'Tracker ID for the data record.',
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
      ],
      'data' => [
        'type' => 'blob',
        'serialize' => TRUE,
      ],
    ],
    'primary key' => ['import_name', 'id'],
  ];

  return $schema;
}
